package Hauptdateien;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import GraphKlassen.Graph;

public class GraphZeichnenFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	Dimension d = this.getToolkit().getScreenSize();
	public GraphZeichnenFrame(Graph graph, Graph matching, int anzahlknoten, String art)  {
		super(art);
		dispose();
		setDisplaygroesse(anzahlknoten);
		setLocation((int) ((d.getWidth() - this.getWidth()) / 2), (int) ((d.getHeight() - this.getHeight()) / 2));
		JPanel panel = new GraphZeichnenPanel(graph, matching, anzahlknoten);
		add(panel, BorderLayout.CENTER);
	}
	public GraphZeichnenFrame(Graph graph, int anzahlknoten, String art)  {
		super(art);
		dispose();
		setDisplaygroesse(anzahlknoten);
		setLocation((int) ((d.getWidth() - this.getWidth()) / 2), (int) ((d.getHeight() - this.getHeight()) / 2));
		JPanel panel = new GraphZeichnenPanel(graph, anzahlknoten);
		panel.setOpaque(true);
		add(panel, BorderLayout.CENTER);
	}
	private void setDisplaygroesse(int anzahlknoten) {
		if(anzahlknoten == 1) {
			setSize(400, 300);
		}
		else if(anzahlknoten == 2) {
			setSize(400, 300);
		}
		else if(anzahlknoten == 3) {
			setSize(440, 440);
		}
		else if(anzahlknoten == 4) {
			setSize(440, 440);
		}
		else if(anzahlknoten == 5) {
			setSize(670, 470);
		}
		else if(anzahlknoten == 6) {
			setSize(670, 470);
		}
		else if(anzahlknoten == 7) {
			setSize(670, 620);
		}
		else if(anzahlknoten == 8) {
			setSize(670, 620);
		}
		else if(anzahlknoten == 9) {
			setSize(950, 750);
		}
		else if(anzahlknoten == 10) {
			setSize(950, 750);
		}
		else if(anzahlknoten == 11) {
			setSize(1200, 1000);
		}
		else if(anzahlknoten == 12) {
			setSize(950, 800);
		}
	}
}
