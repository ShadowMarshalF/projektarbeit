/* AnalyseFrame
 * Dieser Frame bietet die Auswahl aller Algorithmen, das Laden externer Graphen und das oeffnen des AnalyseFrames
 * Geoeffnet wird der Frame beim Start ueber main()-Methode in Matching
 * Methoden stehen keine zur Verfuegung.
 */
package Hauptdateien;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

import ApproxAlgorithmen.*;
import ApproxAlgorithmen.PathGrowingAlgorithmus;
import GraphKlassen.*;
import Beispiele.*;
import Analyse.*;
import EdmondsBlossomAlgorithmus.EdmondsBlossomAlgorithmus;
import Generator.Generator;
import Generator.Generator2;
import Generator.Generator3;
public class Frame extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JMenuBar menu = new JMenuBar();
	private JMenu dateiMenu = new JMenu("Datei");
	private JMenu maxcardapr = new JMenu("Maximum Cardinality - Approx");
	private JMenu maxweigapr = new JMenu("Maximum Weighted - Approx");
	private JMenu maxcard = new JMenu("Maximum Cardinality");
	private JMenu graphzeichnenMenu = new JMenu("Zeichnungen");
	private JMenu analysemenuitem = new JMenu("Analyse");
	private JMenuItem graphLadenItem = new JMenuItem("Graph als .txt einlesen");
	private JMenuItem graphSpeichernItem = new JMenuItem("Graph als .txt speichern");
	private JMenuItem matchingSpeichernItem = new JMenuItem("Matching als .html speichern");
	private JMenuItem weingartAlgoItem = new JMenuItem("Node degree valuation Algorithmus");
	private JMenuItem greedyAlgoItem = new JMenuItem("Greedy Algorithmus");
	private JMenuItem greedyBackAlgoItem = new JMenuItem("Greedy Backwards Algorithmus (GBW)");
	private JMenuItem pathGrowingAlgoItem = new JMenuItem("Path Growing Algorithmus (PG)");
	private JMenuItem pathGrowingAlgoBestWorseItem = new JMenuItem("Path Growing BestWorst Algorithmus (PGBW)");
	private JMenuItem locHeaMatAlgoItem = new JMenuItem("Locally Heaviest Matching Algorithmus (LAM)");
	private JMenuItem edmondsBlossomAlgoItem = new JMenuItem("Edmonds Blossom Algorithmus");
	private JMenuItem sequentialSuitorAlgoItem = new JMenuItem("Sequential Suitor Algorithmus (SSA)");
	private JMenuItem expPathGrowingAlgoItem = new JMenuItem("Expanded Path Growing (EPG)");
	private JMenuItem disjoinedPathGrowingAlgoItem = new JMenuItem("Disjoined Path Growing (DPG)");
	private JMenuItem pathGrowingWithCirclePossibilityAlgoItem = new JMenuItem("Path Growing With Circle Possibility (PGWCP)");
	private JMenuItem disjoinedPathGrowingWithCirclePossibilityAlgoItem = new JMenuItem("Disjoined Path Growing With Circle Possibility (DPGWCP)");
	private JMenuItem analyseItem = new JMenuItem("Algorithmusanlyse");
	private JMenuItem generatorItem = new JMenuItem("Generator 1 - vollständiger Graph");
	private JMenuItem generator2Item = new JMenuItem("Generator 2 - unvollständiger Graph");
	private JMenuItem generator3Item = new JMenuItem("Generator 3 - unvollständiger Graph (mit ID)");
	private JMenuItem example1Car = new JMenuItem("Beispiel 1 - 4 Knoten");
	private JMenuItem example2Car = new JMenuItem("Beispiel 2 - 10 Knoten");
	private JMenuItem example3Car = new JMenuItem("Beispiel 3 - 6 Knoten");
	private JMenuItem example4Car = new JMenuItem("Beispiel 4 - 6 Knoten");
	private JMenuItem example5Car = new JMenuItem("Beispiel 5 - 16 Knoten");
	private JMenuItem example6Car = new JMenuItem("Beispiel 6 - 6 Knoten");
	private JMenuItem example7Car = new JMenuItem("Beispiel 7 - 6 Knoten");
	private JMenuItem example8Car = new JMenuItem("Beispiel 8 - 6 Knoten");
	private JMenuItem example9Car = new JMenuItem("Beispiel 9 - 16 Knoten");
	private JMenuItem example10Car = new JMenuItem("Beispiel 10 - 10 Knoten");
	private JMenuItem example11Car = new JMenuItem("Beispiel 11 - 12 Knoten");
	private JMenuItem example12Car = new JMenuItem("Beispiel 12 - 6 Knoten");
	private JMenuItem example1Wei = new JMenuItem("Beispiel 1 - 4 Knoten");
	private JMenuItem example2Wei = new JMenuItem("Beispiel 2 - 6 Knoten");
	private JMenuItem example3Wei = new JMenuItem("Beispiel 3 - 9 Knoten");
	private JMenuItem example4Wei = new JMenuItem("Beispiel 4 - var. Knoten");
	private JMenuItem example1Edm = new JMenuItem("Beispiel 1 - 8 Knoten");
	private JMenuItem example2Edm = new JMenuItem("Beispiel 2 - 8 Knoten");
	private JMenuItem example3Edm = new JMenuItem("Beispiel 3 - 7 Knoten");
	private JMenuItem example4Edm = new JMenuItem("Beispiel 4 - 8 Knoten");
	private JMenuItem example5Edm = new JMenuItem("Beispiel 5 - 10 Knoten");
	private JMenuItem example6Edm = new JMenuItem("Beispiel 6 - 4 Knoten");
	private JMenuItem graphzeichnen = new JMenuItem("Graph zeichnen ( <= 12 Knoten )");
	private JMenuItem matchingzeichnen = new JMenuItem("Matching zeichnen ( <= 12 Knoten )");
	private JLabel infolabel = new JLabel("Informationen");
	private JLabel graph = new JLabel("Graph",JLabel.CENTER);
	private JLabel matching = new JLabel("Kanten",JLabel.CENTER);
	private JLabel matchingGraph = new JLabel("Matching",JLabel.CENTER);
	private JPanel layout = new JPanel();
	private File file;
	
	private NodeDegreeValuationAlgorithmus wa;
	private GreedyAlgorithmus ga;
	private GreedyAlgorithmusBackwards gba;
	private PathGrowingAlgorithmus pa;
	private LocallyHeaviestMatchingAlgorithmus lam;
	private EdmondsBlossomAlgorithmus eda;
	private PathGrowingAlgorithmusBestWorst pgbw;
	private SequentialSuitorAlgorithmus ssa;
	private ExpandedPathGrowingAlgorithmus epg;
	private DisjoinedPathGrowing dpg;
	private PathGrowingWithCirclePossibility pgwcp;
	private DisjoinedPathGrowingWithCirclePossibility dpgwcp;
	private Graph g;
	private Graph m;
	private String graphSpeichern;
	private String matchingSpeichern;
	private Beispiele beispiele;
	Dimension d = this.getToolkit().getScreenSize();
	public Frame() {
		super("Matching");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(1200, 600));
		setLocation((int) ((d.getWidth() - this.getWidth()) / 2), (int) ((d.getHeight() - this.getHeight()) / 2));
		layout.setLayout(new GridLayout(1,3));
		layout.add(graph);
		layout.add(matchingGraph);
		layout.add(matching);
		add(layout, BorderLayout.CENTER);
		add(infolabel, BorderLayout.PAGE_END);
		dateiMenu.add(graphLadenItem);
		dateiMenu.add(graphSpeichernItem);
		dateiMenu.add(matchingSpeichernItem);
		graphzeichnenMenu.add(graphzeichnen);
		graphzeichnenMenu.add(matchingzeichnen);
		menu.add(dateiMenu);
		menu.add(maxcardapr);
		menu.add(maxcard);
		menu.add(maxweigapr);
		menu.add(graphzeichnenMenu);
		menu.add(analysemenuitem);
		analysemenuitem.add(analyseItem);
		analysemenuitem.add(new JSeparator());
		analysemenuitem.add(generatorItem);
		analysemenuitem.add(generator2Item);
		analysemenuitem.add(generator3Item);
		maxcardapr.add(weingartAlgoItem);
		maxcardapr.add(new JSeparator());
		maxcardapr.add(example1Car);
		maxcardapr.add(example2Car);
		maxcardapr.add(example3Car);
		maxcardapr.add(example4Car);
		maxcardapr.add(example5Car);
		maxcardapr.add(example6Car);
		maxcardapr.add(example7Car);
		maxcardapr.add(example8Car);
		maxcardapr.add(example9Car);
		maxcardapr.add(example10Car);
		maxcardapr.add(example11Car);
		maxcardapr.add(example12Car);
				
		maxweigapr.add(greedyAlgoItem);
		maxweigapr.add(pathGrowingAlgoItem);
		maxweigapr.add(locHeaMatAlgoItem);
		maxweigapr.add(sequentialSuitorAlgoItem);
		maxweigapr.add(new JSeparator());
		maxweigapr.add(greedyBackAlgoItem);
		maxweigapr.add(pathGrowingAlgoBestWorseItem);
		maxweigapr.add(expPathGrowingAlgoItem);
		maxweigapr.add(disjoinedPathGrowingAlgoItem);
		maxweigapr.add(pathGrowingWithCirclePossibilityAlgoItem);
		maxweigapr.add(disjoinedPathGrowingWithCirclePossibilityAlgoItem);
		maxweigapr.add(new JSeparator());
		maxweigapr.add(example1Wei);
		maxweigapr.add(example2Wei);
		maxweigapr.add(example3Wei);
		maxweigapr.add(example4Wei);
		
		maxcard.add(edmondsBlossomAlgoItem);
		maxcard.add(new JSeparator());
		maxcard.add(example1Edm);
		maxcard.add(example2Edm);
		maxcard.add(example3Edm);
		maxcard.add(example4Edm);
		maxcard.add(example5Edm);
		maxcard.add(example6Edm);
		graphLadenItem.addActionListener(this);
		matchingSpeichernItem.addActionListener(this);
		weingartAlgoItem.addActionListener(this);
		greedyAlgoItem.addActionListener(this);
		greedyBackAlgoItem.addActionListener(this);
		pathGrowingAlgoItem.addActionListener(this);
		locHeaMatAlgoItem.addActionListener(this);
		edmondsBlossomAlgoItem.addActionListener(this);
		example1Car.addActionListener(this);
		example2Car.addActionListener(this);
		example3Car.addActionListener(this);
		example4Car.addActionListener(this);
		example5Car.addActionListener(this);
		example6Car.addActionListener(this);
		example7Car.addActionListener(this);
		example8Car.addActionListener(this);
		example9Car.addActionListener(this);
		example10Car.addActionListener(this);
		example11Car.addActionListener(this);
		example12Car.addActionListener(this);
		example1Wei.addActionListener(this);
		example2Wei.addActionListener(this);
		example3Wei.addActionListener(this);
		example4Wei.addActionListener(this);
		example1Edm.addActionListener(this);
		example2Edm.addActionListener(this);
		example3Edm.addActionListener(this);
		example4Edm.addActionListener(this);
		example5Edm.addActionListener(this);
		example6Edm.addActionListener(this);
		graphzeichnen.addActionListener(this);
		matchingzeichnen.addActionListener(this);
		analyseItem.addActionListener(this);
		generatorItem.addActionListener(this);
		generator2Item.addActionListener(this);
		generator3Item.addActionListener(this);
		graphSpeichernItem.addActionListener(this);
		pathGrowingAlgoBestWorseItem.addActionListener(this);
		sequentialSuitorAlgoItem.addActionListener(this);
		expPathGrowingAlgoItem.addActionListener(this);
		disjoinedPathGrowingAlgoItem.addActionListener(this);
		disjoinedPathGrowingWithCirclePossibilityAlgoItem.addActionListener(this);
		pathGrowingWithCirclePossibilityAlgoItem.addActionListener(this);
		setJMenuBar(menu);
		beispiele = new Beispiele();
		this.g = null;
		this.m = null;
	}
	public void actionPerformed (ActionEvent ae) {
		if(ae.getSource() == this.graphLadenItem) {
			try {
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				file = chooser.getSelectedFile();
				beispieleLaden(file);
			}catch(Exception e) {
				
			}
		}
		else if(ae.getSource() == this.example1Car) {
			beispieleLaden2(beispiele.getGraphbsp1car());
		}
		else if(ae.getSource() == this.example2Car) {
			beispieleLaden2(beispiele.getGraphbsp2car());
		}
		else if(ae.getSource() == this.example3Car) {
			beispieleLaden2(beispiele.getGraphbsp3car());
		}
		else if(ae.getSource() == this.example4Car) {
			beispieleLaden2(beispiele.getGraphbsp4car());
		}
		else if(ae.getSource() == this.example5Car) {
			beispieleLaden2(beispiele.getGraphbsp5car());
		}
		else if(ae.getSource() == this.example6Car) {
			beispieleLaden2(beispiele.getGraphbsp6car());
		}
		else if(ae.getSource() == this.example7Car) {
			beispieleLaden2(beispiele.getGraphbsp7car());
		}
		else if(ae.getSource() == this.example8Car) {
			beispieleLaden2(beispiele.getGraphbsp8car());
		}
		else if(ae.getSource() == this.example9Car) {
			beispieleLaden2(beispiele.getGraphbsp9car());
		}
		else if(ae.getSource() == this.example10Car) {
			beispieleLaden2(beispiele.getGraphbsp10car());
		}
		else if(ae.getSource() == this.example11Car) {
			beispieleLaden2(beispiele.getGraphbsp11car());
		}
		else if(ae.getSource() == this.example12Car) {
			beispieleLaden2(beispiele.getGraphbsp12car());
		}
		else if(ae.getSource() == this.example1Wei) {
			beispieleLaden2(beispiele.getGraphbsp1wei());
		}
		else if(ae.getSource() == this.example2Wei) {
			beispieleLaden2(beispiele.getGraphbsp2wei());
		}
		else if(ae.getSource() == this.example3Wei) {
			beispieleLaden2(beispiele.getGraphbsp3wei());
		}
		else if(ae.getSource() == this.example4Wei) {
			beispieleLaden2(beispiele.getGraphbsp4wei());
		}
		else if(ae.getSource() == this.example1Edm) {
			beispieleLaden3(beispiele.getGraphbsp1edm(), beispiele.getMatchingsp1edm());
		}
		else if(ae.getSource() == this.example2Edm) {
			beispieleLaden3(beispiele.getGraphbsp2edm(), beispiele.getMatchingsp2edm());
		}
		else if(ae.getSource() == this.example3Edm) {
			beispieleLaden3(beispiele.getGraphbsp3edm(), beispiele.getMatchingsp3edm());
		}
		else if(ae.getSource() == this.example4Edm) {
			beispieleLaden3(beispiele.getGraphbsp4edm(), beispiele.getMatchingsp4edm());
		}
		else if(ae.getSource() == this.example5Edm) {
			beispieleLaden3(beispiele.getGraphbsp5edm(), beispiele.getMatchingsp5edm());
		}
		else if(ae.getSource() == this.example6Edm) {
			beispieleLaden3(beispiele.getGraphbsp6edm(), beispiele.getMatchingsp6edm());
		}
		else if(ae.getSource() == this.analyseItem) {
			JFrame analyse = new AnalyseFrame();
			analyse.setVisible(true);
		}
		else if(ae.getSource() == this.graphzeichnen) {
			if(this.g.getAnzahlKnoten() > 12)
				JOptionPane.showMessageDialog(null, "Der Graph besitzt zum Anzeigen zu viele Knoten!");
			else if(this.g != null) {
				GraphZeichnenFrame gz = null;
				gz = new GraphZeichnenFrame(this.g, this.g.getAnzahlKnoten(), "Graph");
				gz.setVisible(true);
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden!");
		}
		else if(ae.getSource() == this.matchingzeichnen) {
			if(this.g.getAnzahlKnoten() > 12)
				JOptionPane.showMessageDialog(null, "Der Graph & das Matching besitzen zum Anzeigen zu viele Knoten!");
			else if(this.g != null && this.m != null) {
				GraphZeichnenFrame gz = null;
				gz = new GraphZeichnenFrame(this.g, this.m, this.g.getAnzahlKnoten(), "Matching-Graph");
				gz.setVisible(true);
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst ein Matching generieren!");		
		}
		else if(ae.getSource() == this.weingartAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.wa = new NodeDegreeValuationAlgorithmus(this.g);
				long zeit = System.currentTimeMillis() - start;
				this.m = wa.getMatchingGraph();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + wa.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + wa.writeErgebnis() +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + wa.writeTableHTML() +"</body></html>");
				}
				else {
					wa.writeErgebnis();
				}
				infolabel.setText(wa.getMatchingInfo() + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.greedyAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.ga = new GreedyAlgorithmus(g.getAnzahlKnoten(), g.getKantenliste());
				long zeit = System.currentTimeMillis() - start;
				this.m = ga.getMatchingGraph();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + ga.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + ga.writeErgebnis() +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + ga.writeTableHTML() +"</body></html>");
				}
				else {
					this.ga.writeErgebnis();
				}
				infolabel.setText("Greedy berechnet mit Gesamtgewichtswert: " + ga.getMatchingGewicht() + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden\n bevor Sie diesen Algorithmus ausführen können!");
		
		}
		else if(ae.getSource() == this.greedyBackAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.gba = new GreedyAlgorithmusBackwards(g.getAnzahlKnoten(), g);
				long zeit = System.currentTimeMillis() - start;
				this.m = gba.getMatchingGraph();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + gba.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + gba.writeErgebnis() +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + gba.writeTableHTML() +"</body></html>");
				}
				else {
					this.gba.writeErgebnis();
				}
				infolabel.setText("GBW berechnet mit Gesamtgewichtswert: " + gba.getMatchingGewicht() + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden\n bevor Sie diesen Algorithmus ausführen können!");
		
		}
		else if(ae.getSource() == this.pathGrowingAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.pa = new PathGrowingAlgorithmus(this.g);
				long zeit = System.currentTimeMillis() - start;
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + pa.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + pa.writeErgebnis() +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + pa.writeTableHTML() +"</body></html>");
				}
				else {
					this.pa.writeErgebnis();
				}
				infolabel.setText("PathGrowing berechnet mit Gesamtgewichtswert: " + pa.getMatchingGewicht() + ". Berechnungsdauer: " + (zeit) + "ms");
				this.m = pa.getMatchingGraph();
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.pathGrowingAlgoBestWorseItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.pgbw = new PathGrowingAlgorithmusBestWorst(this.g);
				long zeit = System.currentTimeMillis() - start;
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + pgbw.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + pgbw.writeErgebnis() +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + pgbw.writeTableHTML() +"</body></html>");
				}
				else {
					this.pgbw.writeErgebnis();
				}
				infolabel.setText("PGBW berechnet mit Gesamtgewichtswert: " + pgbw.getMatchingGewicht() + ". Berechnungsdauer: " + (zeit) + "ms");
				this.m = pgbw.getMatchingGraph();
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.locHeaMatAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.lam = new LocallyHeaviestMatchingAlgorithmus(this.g);
				long zeit = System.currentTimeMillis() - start;
				this.m = lam.getMatchingGraph();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + lam.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + lam.writeErgebnis() +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + lam.writeTableHTML() +"</body></html>");
				}
				else {
					this.lam.writeErgebnis();
				}
				infolabel.setText("LAM berechnet mit Gesamtgewichtswert: " + lam.getMatchingGewicht()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.edmondsBlossomAlgoItem) {
			if(this.g != null && this.m != null) {
				long start = System.currentTimeMillis();
				this.eda = new EdmondsBlossomAlgorithmus(g.getGraph(), m.getGraph(), g.getAnzahlKnoten());
				long zeit = System.currentTimeMillis() - start;
				Graph edag = eda.berechneMatching();
				this.m = edag;
				//this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + eda.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M:<br><br>" + edag.writeTableHTML() +"</body></html>");
				}
				infolabel.setText(eda.getErgebnis()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden und ein Matching generieren\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.sequentialSuitorAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.ssa = new SequentialSuitorAlgorithmus(this.g);
				long zeit = System.currentTimeMillis() - start;
				Graph edag = ssa.getMatchingGraph();
				this.m = edag;
				String ergebnis = ssa.writeErgebnis();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + ssa.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + ergebnis +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + ssa.writeTableHTML() +"</body></html>");
				}
				infolabel.setText("SSA berechnet mit Gesamtgewichtswert: " + ssa.getMatchingGewicht()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden und ein Matching generieren\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.expPathGrowingAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.epg = new ExpandedPathGrowingAlgorithmus(this.g);
				long zeit = System.currentTimeMillis() - start;
				Graph edag = epg.getMatchingGraph();
				this.m = edag;
				String ergebnis = epg.writeErgebnis();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + epg.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + ergebnis +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + epg.writeTableHTML() +"</body></html>");
				}
				infolabel.setText("EPG berechnet mit Gesamtgewichtswert: " + epg.getMatchingGewicht()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden und ein Matching generieren\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.disjoinedPathGrowingAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.dpg = new DisjoinedPathGrowing(this.g);
				long zeit = System.currentTimeMillis() - start;
				Graph edag = dpg.getMatchingGraph();
				this.m = edag;
				String ergebnis = dpg.writeErgebnis();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + dpg.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + ergebnis +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + dpg.writeTableHTML() +"</body></html>");
				}
				infolabel.setText("DPG berechnet mit Gesamtgewichtswert: " + dpg.getMatchingGewicht()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden und ein Matching generieren\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.pathGrowingWithCirclePossibilityAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.pgwcp = new PathGrowingWithCirclePossibility(this.g);
				long zeit = System.currentTimeMillis() - start;
				Graph edag = pgwcp.getMatchingGraph();
				this.m = edag;
				String ergebnis = pgwcp.writeErgebnis();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + pgwcp.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + ergebnis +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + pgwcp.writeTableHTML() +"</body></html>");
				}
				infolabel.setText("PGWCP berechnet mit Gesamtgewichtswert: " + pgwcp.getMatchingGewicht()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden und ein Matching generieren\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.disjoinedPathGrowingWithCirclePossibilityAlgoItem) {
			if(this.g != null) {
				long start = System.currentTimeMillis();
				this.dpgwcp = new DisjoinedPathGrowingWithCirclePossibility(this.g);
				long zeit = System.currentTimeMillis() - start;
				Graph edag = dpgwcp.getMatchingGraph();
				this.m = edag;
				String ergebnis = dpgwcp.writeErgebnis();
				this.matchingSpeichern = "<html><body>Das Matching M:<br><br>" + dpgwcp.writeTableHTML() +"</body></html>";
				if(this.g.getAnzahlKnoten() <20) {
					matching.setText("<html><body>Das Matching M zum Graphen G:<br><br>" + ergebnis +"</body></html>");
					matchingGraph.setText("<html><body>Das Matching M:<br><br>" + dpgwcp.writeTableHTML() +"</body></html>");
				}
				infolabel.setText("DPGWCP berechnet mit Gesamtgewichtswert: " + dpgwcp.getMatchingGewicht()  + ". Berechnungsdauer: " + (zeit) + "ms");
			}
			else
				JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen laden und ein Matching generieren\n bevor Sie diesen Algorithmus ausführen können!");
		}
		else if(ae.getSource() == this.matchingSpeichernItem) {
			try {
				JFileChooser chooser = new JFileChooser();
				chooser.showSaveDialog(null);
				File file = chooser.getSelectedFile();
				FileWriter fw = new FileWriter(file);
				//fw.write("Der Original-Graph\n");
				//fw.write(graphSpeichern);
				//fw.write("Der gemachte Graph\n");
				fw.write(matchingSpeichern);
				fw.flush();
				fw.close();
				infolabel.setText("Datei erfolgreich gespeichert!");
			}catch(Exception e) {
				System.out.println(e);
			}
		}
		else if(ae.getSource() == this.graphSpeichernItem) {
			try {
				JFileChooser chooser = new JFileChooser();
				chooser.showSaveDialog(null);
				File file = chooser.getSelectedFile();
				FileWriter fw = new FileWriter(file);
				fw.write(this.g.getAnzahlKnoten()+"\n");
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					for(int j = i+1; j < this.g.getAnzahlKnoten(); j++) {
						if(this.g.getGraph()[i][j] != 0) {
							fw.write(i+1 + ",");
							fw.write(j+1 + ",");
							fw.write(this.g.getGraph()[i][j]+"\n");
						}
					}
				}
				fw.flush();
				fw.close();
				infolabel.setText("Datei erfolgreich gespeichert!");
			}catch(Exception e) {
				
			}
		}
		else if(ae.getSource() == this.generatorItem) {
			try {
				try {
					JFileChooser chooser = new JFileChooser();
					chooser.showOpenDialog(null);
					file = chooser.getSelectedFile();
				}catch(Exception e) {
					
				}
				Generator gen = new Generator(file);
				setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
				gen.berechneGraph();
				this.g = gen.getGraph();
				this.m = null;
				if(this.g.getAnzahlKnoten() <20) {
					graphSpeichern =g.writeTable();
					graph.setText("<html><body>Der Graph G:<br><br>" + g.writeTableHTML() +"</body></html>");
				}
				matching.setText("Kanten");
				matchingGraph.setText("Matching");
				infolabel.setText(file.getName() + " geladen");
				setCursor(Cursor.getDefaultCursor());
				
			}catch(Exception e) {
				
			}
		}
		else if(ae.getSource() == this.generator2Item) {
			try {
				try {
					JFileChooser chooser = new JFileChooser();
					chooser.showOpenDialog(null);
					file = chooser.getSelectedFile();
				}catch(Exception e) {
					
				}
				Generator2 gen = new Generator2(file);
				setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
				gen.berechneGraph();
				this.g = gen.getGraph();
				this.m = null;
				graphSpeichern =g.writeTable();
				if(this.g.getAnzahlKnoten() <20) {
					graph.setText("<html><body>Der Graph G:<br><br>" + g.writeTableHTML() +"</body></html>");
				}
				matching.setText("Kanten");
				matchingGraph.setText("Matching");
				infolabel.setText(file.getName() + " geladen");
				setCursor(Cursor.getDefaultCursor());
			}catch(Exception e) {
				
			}
		}
		else if(ae.getSource() == this.generator3Item) {
			try {
				try {
					JFileChooser chooser = new JFileChooser();
					chooser.showOpenDialog(null);
					file = chooser.getSelectedFile();
				}catch(Exception e) {
					
				}
				Generator3 gen = new Generator3(file);
				setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
				gen.berechneGraph();
				this.g = gen.getGraph();
				this.m = null;
				graphSpeichern =g.writeTable();
				if(this.g.getAnzahlKnoten() <20) {
					graph.setText("<html><body>Der Graph G:<br><br>" + g.writeTableHTML() +"</body></html>");
				}
				matching.setText("Kanten");
				matchingGraph.setText("Matching");
				infolabel.setText(file.getName() + " geladen");
				setCursor(Cursor.getDefaultCursor());
			}catch(Exception e) {
				
			}
		}
	}
	
	private void beispieleLaden(File file) {
		try {
			GraphLaden gl = new GraphLaden(file);
			setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
			this.g = gl.getGraph();
			this.m = null;
			graphSpeichern =g.writeTable();
			if(this.g.getAnzahlKnoten() <20) {
				graph.setText("<html><body>Der Graph G:<br><br>" + g.writeTableHTML() +"</body></html>");
			}
			matching.setText("Kanten");
			matchingGraph.setText("Matching");
			infolabel.setText(file.getName() + " geladen");
			setCursor(Cursor.getDefaultCursor());
		}catch(Exception e) {
			
		}
	}
	private void beispieleLaden2(Graph graphL) {
		try {
			this.g = graphL;
			this.m = null;
			graphSpeichern =g.writeTable();
			if(this.g.getAnzahlKnoten() <20) {
				graph.setText("<html><body>Der Graph G:<br><br>" + g.writeTableHTML() +"</body></html>");
			}
			matching.setText("Kanten");
			matchingGraph.setText("Matching");
			infolabel.setText("Beispiel geladen");
			setCursor(Cursor.getDefaultCursor());
		}catch(Exception e) {
			
		}
	}
	private void beispieleLaden3(Graph graphL, Graph matchingL) {
		try {
			this.g = graphL;
			this.m = matchingL;
			graphSpeichern =g.writeTable();
			if(this.g.getAnzahlKnoten() <20) {
				graph.setText("<html><body>Der Graph G:<br><br>" + g.writeTableHTML() +"</body></html>");
			}
			matchingGraph.setText("<html><body>Das eventuell zu <br>verbessernde Matching M:<br><br>" + m.writeTableHTML() +"</body></html>");
			matching.setText("Matching");
			infolabel.setText("Beispiel & Matching geladen");
		}catch(Exception e) {
			
		}
	}
}
