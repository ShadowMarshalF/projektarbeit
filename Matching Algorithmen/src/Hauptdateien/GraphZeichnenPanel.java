package Hauptdateien;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;

import javax.swing.JPanel;

import GraphKlassen.Graph;
import GraphKlassen.Kante;

public class GraphZeichnenPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private int anzahlknoten;
	private ArrayList<Kante> kantenliste = new ArrayList<Kante>();
	private ArrayList<Kante> kantenlisteM = new ArrayList<Kante>();
	public GraphZeichnenPanel(Graph graph, Graph matching, int anzahlknoten)  {
		this.anzahlknoten = anzahlknoten;
		this.kantenliste = graph.getKantenliste();
		this.kantenlisteM = matching.getKantenliste();
	}
	public GraphZeichnenPanel(Graph graph, int anzahlknoten)  {
		this.anzahlknoten = anzahlknoten;
		this.kantenliste = graph.getKantenliste();
		this.kantenlisteM = null;
	}
	public void paint(Graphics g) {
		int[] xWerte = null;
		int[] yWerte = null;
		Graphics2D graphic = (Graphics2D) g;
		graphic.setColor(Color.black);
		graphic.setStroke(new BasicStroke(1));
		if(this.anzahlknoten == 1) {
			xWerte = new int[]{100};
			yWerte = new int[]{100};
		}
		else if(this.anzahlknoten == 2) {
			xWerte = new int[]{100, 100};
			yWerte = new int[]{100, 300};
		}
		else if(this.anzahlknoten == 3) {
			xWerte = new int[]{100, 300, 100};
			yWerte = new int[]{100, 100, 300};
		}
		else if(this.anzahlknoten == 4) {
			xWerte = new int[]{100, 300, 100, 300};
			yWerte = new int[]{100, 100, 300, 300};
		}
		else if(this.anzahlknoten == 5) {
			xWerte = new int[]{50, 200, 200, 400, 400};
			yWerte = new int[]{200, 100, 300, 100, 300};
		}
		else if(this.anzahlknoten == 6) {
			xWerte = new int[]{50, 200, 200, 400, 400, 550};
			yWerte = new int[]{200, 100, 300, 100, 300, 200};
		}
		else if(this.anzahlknoten == 7) {
			xWerte = new int[]{50, 200, 400, 550, 550, 400, 200};
			yWerte = new int[]{200, 100, 100, 200, 400, 500, 500};
		}
		else if(this.anzahlknoten == 8) {
			xWerte = new int[]{50, 200, 400, 550, 550, 400, 200, 50};
			yWerte = new int[]{200, 100, 100, 200, 400, 500, 500, 400};
		}
		else if(this.anzahlknoten == 9) {
			xWerte = new int[]{50, 100, 200, 300, 400, 450, 400, 300, 200};
			yWerte = new int[]{350, 200, 100, 100, 200, 350, 500, 600, 600};
		}
		else if(this.anzahlknoten == 10) {
			xWerte = new int[]{50, 200, 350, 500, 650, 800, 650, 500, 350, 200};
			yWerte = new int[]{350, 150, 100, 100, 150, 350, 550, 600, 600, 550};
		}
		else if(this.anzahlknoten == 11) {
			xWerte = new int[]{50, 150, 300, 550, 700, 800, 800, 700, 550, 300, 150};
			yWerte = new int[]{250, 100, 50, 50, 100, 250, 450, 600, 650, 650, 600};
		}
		else if(this.anzahlknoten == 12) {
			xWerte = new int[]{50, 150, 300, 550, 700, 800, 800, 700, 550, 300, 150, 50};
			yWerte = new int[]{250, 100, 50, 50, 100, 250, 450, 600, 650, 650, 600, 450};
		}
		if(xWerte != null && yWerte != null) {
			graphic.setFont(new Font("Arial",Font.BOLD,18));
			for(int i = 0; i < this.anzahlknoten; i++) {
				graphic.drawOval(xWerte[i], yWerte[i], 50, 50);
				if(i < 9)
					graphic.drawString((i+1) + "", xWerte[i]+20, yWerte[i]+30);
				else
					graphic.drawString((i+1) + "", xWerte[i]+15, yWerte[i]+30);
			}
			graphic.setFont(new Font("Arial",Font.BOLD,13));
			for(int i = 0; i < this.kantenliste.size(); i++) {
				int ersterknoten = kantenliste.get(i).getKnoten1()-1;
				int zweiterknoten = kantenliste.get(i).getKnoten2()-1;
				int gewicht = kantenliste.get(i).getGewicht();
				graphic.drawLine(xWerte[ersterknoten]+25, yWerte[ersterknoten]+25, xWerte[zweiterknoten]+25, yWerte[zweiterknoten]+25);
				if(xWerte[ersterknoten] == xWerte[zweiterknoten])
					graphic.drawString(""+gewicht, xWerte[zweiterknoten]+35, (yWerte[zweiterknoten]+yWerte[ersterknoten])/2+25);
				else if(yWerte[ersterknoten] == yWerte[zweiterknoten])
					graphic.drawString(""+gewicht, (xWerte[zweiterknoten]+xWerte[ersterknoten])/2+20, yWerte[ersterknoten]+45);
				else 
					graphic.drawString(""+gewicht, (xWerte[zweiterknoten]+xWerte[ersterknoten])/2+35, (yWerte[zweiterknoten]+yWerte[ersterknoten])/2+35);
			}
			if(kantenlisteM != null) {
				graphic.setColor(Color.red);
				graphic.setFont(new Font("Arial",Font.BOLD,13));
				for(int i = 0; i < this.kantenlisteM.size(); i++) {
					int ersterknoten = kantenlisteM.get(i).getKnoten1()-1;
					int zweiterknoten = kantenlisteM.get(i).getKnoten2()-1;
					Stroke dashed = new BasicStroke(4, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{20}, 0); 
					graphic.setStroke(dashed);
					graphic.drawLine(xWerte[ersterknoten]+25, yWerte[ersterknoten]+25, xWerte[zweiterknoten]+25, yWerte[zweiterknoten]+25);
					int gewicht = kantenlisteM.get(i).getGewicht();
					graphic.drawLine(xWerte[ersterknoten]+25, yWerte[ersterknoten]+25, xWerte[zweiterknoten]+25, yWerte[zweiterknoten]+25);
					if(xWerte[ersterknoten] == xWerte[zweiterknoten])
						graphic.drawString(""+gewicht, xWerte[zweiterknoten]+35, (yWerte[zweiterknoten]+yWerte[ersterknoten])/2+25);
					else if(yWerte[ersterknoten] == yWerte[zweiterknoten])
						graphic.drawString(""+gewicht, (xWerte[zweiterknoten]+xWerte[ersterknoten])/2+20, yWerte[ersterknoten]+45);
					else 
						graphic.drawString(""+gewicht, (xWerte[zweiterknoten]+xWerte[ersterknoten])/2+35, (yWerte[zweiterknoten]+yWerte[ersterknoten])/2+35);
				}
			}
		}
	}
}
