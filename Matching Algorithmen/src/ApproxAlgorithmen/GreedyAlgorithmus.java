/* Greedy Algorithmus
 * Bietet den Konstruktor GreedyAlgorithmus(int anzahlKnoten, ArrayList<Kante> kantenliste)
 * generiereMatching(): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben.
 * fuelleMaxHeap(): Der Max Heap wird anhand der Kantengewichte gefuellt
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 */
package ApproxAlgorithmen;
import java.util.ArrayList;
import GraphKlassen.*;
public class GreedyAlgorithmus {
	private int anzahlKnoten;
	private ArrayList<Kante> kantenliste;
	private ArrayList<Kante> matchingKanten;
	private MaxHeap Kanten;
	private boolean kanteVerfuegbar[];
	private int matchingGewicht = 0;
	public GreedyAlgorithmus(int anzahlKnoten, ArrayList<Kante> kantenliste) {
		this.anzahlKnoten = anzahlKnoten;
		this.kantenliste = new ArrayList<Kante>();
		for(int i = 0; i < kantenliste.size(); i++) {
			this.kantenliste.add(kantenliste.get(i));
		}
		this.matchingKanten = new ArrayList<Kante>();
		Kanten = new MaxHeap(anzahlKnoten*anzahlKnoten);
		kanteVerfuegbar = new boolean[anzahlKnoten];
		fuelleMaxHeap();
		berechneMatching();
	}
	private void fuelleMaxHeap() {
		for(int i = 0; i < kantenliste.size(); i++) {
			Kanten.insert(kantenliste.get(i));
			
		}
	}
	private void berechneMatching() {
		Kante kante;
		while((kante = Kanten.extractMax()) != null) {
			if(!kanteVerfuegbar[kante.getKnoten1()-1] && !kanteVerfuegbar[kante.getKnoten2()-1]) {
				matchingKanten.add(kante);
				kanteVerfuegbar[kante.getKnoten1()-1] = true;
				kanteVerfuegbar[kante.getKnoten2()-1] = true;
			}
		}
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe = new Graph(this.anzahlKnoten);
		for(int i = 0; i < this.matchingKanten.size(); i++) {
			rueckgabe.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		
		return rueckgabe;
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i)+"</tr>";
			this.matchingGewicht+= matchingKanten.get(i).getGewicht();
		}
		rueckgabe += "</table>";
		return rueckgabe;
	}
	public int getMatchingGewicht() {
		return matchingGewicht;
	}
	public String writeTableHTML() {
		Graph matching = new Graph(anzahlKnoten);
		for(int i = 0; i < matchingKanten.size();i++)
			matching.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
}
