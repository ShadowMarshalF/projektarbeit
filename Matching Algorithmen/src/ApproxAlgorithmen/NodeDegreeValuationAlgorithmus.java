/* Node Degree Valuation Algorithmus
 * Bietet den Konstruktor NodeDegreeValuationAlgorithmus(Graph g)
 * generiereMatching(): Ist die eigentliche Berechnung des Algorithmus, wie in der Seminararbeit beschrieben.
 * bewertungDerKnoten(): Fuer jeden Knoten wird der Grad bestimmt 
 * waehleKnoten(): Der Knoten mit der geringsten Bewertung wird gewaehlt
 * verbindeKnoten(): Der Knoten mit der geringsten Bewertung mit Verbindung zu dem Knoten aus waehleKnoten() wird gewaehlt
 * loscheKnoten(): Die gewaehlten Knoten werden zum Matching hunzugefuegt, aus den moeglichen Knoten geloescht und
 * 						die Bewertung wird angepasst.
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 */
package ApproxAlgorithmen;
import java.util.ArrayList;
import GraphKlassen.*;
public class NodeDegreeValuationAlgorithmus {
	private int anzahl_knoten;
	private int[][] graph;
	private int[] bewertung;
	private Graph matching;
	private ArrayList<Kante> matchingKanten;
	public NodeDegreeValuationAlgorithmus(Graph g) {
		anzahl_knoten = g.getAnzahlKnoten();
		this.graph = new int[anzahl_knoten][anzahl_knoten];
		for(int i = 0; i<anzahl_knoten; i++) {
			for(int j = 0; j < anzahl_knoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		bewertung = new int[anzahl_knoten];
		matching = new Graph(anzahl_knoten);
		this.matchingKanten = new ArrayList<Kante>();
		generiereMatching();
	}

	private void bewertungDerKnoten() {
		for(int i = 0; i<anzahl_knoten; i++) {
			int anzahl = 0;
			for(int j = 0; j<anzahl_knoten; j++) {
				if(graph[i][j]>0) {
					anzahl += 1;
				}
				bewertung[i] = anzahl;
			}
		}
	}
	private int waehleKnoten() {
		int rueckgabeKnoten = 0;
		int groessterWert = anzahl_knoten;
		for(int i = 0; i < anzahl_knoten; i++) {
			if(bewertung[i]<groessterWert&&bewertung[i]>0) {
				groessterWert = bewertung[i];
				rueckgabeKnoten = i+1;
			}
		}
		return rueckgabeKnoten;
	}
	private int verbindeKnoten(int knoten1) {
		int rueckgabeKnoten = 0;
		int bewertungTemp = 0;
		for(int i = 0; i< anzahl_knoten; i++) {
			if(graph[knoten1-1][i]>0) {
				if(bewertung[i]>=bewertungTemp) {
					rueckgabeKnoten = i+1;
					bewertungTemp = bewertung[i];
				}
			}
		}
		return rueckgabeKnoten;
	}
	private void loescheKnoten(int knoten1, int knoten2) {
		for(int i = 0; i<anzahl_knoten; i++) {
			if(graph[knoten1-1][i] > 0)
				bewertung[i] -= 1;
			if(graph[knoten2-1][i] > 0)
				bewertung[i] -= 1;
			graph[knoten1-1][i] = 0;
			graph[knoten2-1][i] = 0;
			graph[i][knoten1-1] = 0;
			graph[i][knoten2-1] = 0;
		}
		bewertung[knoten1-1] = 0;
		bewertung[knoten2-1] = 0;
	}
	private void generiereMatching() {
		bewertungDerKnoten();
		int knoten1 = waehleKnoten();
		while(knoten1>0) {
			int knoten2 = verbindeKnoten(knoten1);
			matching.insertKante(knoten1, knoten2, 1);
			loescheKnoten(knoten1, knoten2);
			matchingKanten.add(new Kante(knoten1, knoten2, 1));
			knoten1 = waehleKnoten();
		}
	}
	public String getMatchingInfo() {
		String ausgabe;
		int[][] m = matching.getGraph();
		int anzahl = 0;
		for(int i = 0; i < anzahl_knoten; i++) {
			for(int j = 0; j < anzahl_knoten; j++) {
				if(j>i && m[i][j]>0) {
					anzahl +=1;
				}
			}
		}
		if(2*anzahl == anzahl_knoten) {
			ausgabe = "Perfektes Matching generiert!";
		}
		else {
			ausgabe = "Maximum Matching generiert! " + ((anzahl_knoten/2)-(anzahl-1)) + " Knoten noch frei!";
		}
		return ausgabe;
	}
	public String erhalteMatchKnoten() {
		String ausgabe = "";
		int[][] m = matching.getGraph();
		for(int i = 0; i < anzahl_knoten; i++) {
			for(int j = 0; j < anzahl_knoten; j++) {
				if(j>i && m[i][j]>0) {
					ausgabe += (i+1) + " & " + (j+1) + "\n";
				}
			}
		}
		return ausgabe;
	}
	public Graph getMatchingGraph() {
		return this.matching;
	}
	public String writeTable() {
		String ausgabe="";
		int[][] m = matching.getGraph();
		for(int i = 0; i<anzahl_knoten; i++) {
			for(int j = 0; j<anzahl_knoten; j++) {
				ausgabe += m[i][j]+" ";
			}
			ausgabe+="\n";
		}
		return ausgabe;
	}
	public String writeTableHTML() {
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahl_knoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahl_knoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahl_knoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td>";
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i).toString2()+"</tr>";
		}
		rueckgabe += "</table>";
		return rueckgabe;
	}
}
