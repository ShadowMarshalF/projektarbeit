/* Locally Heaviest Matching Algorithmus
 * Bietet die beiden Konstruktoren LocallyHeaviestMatchingAlgorithmus(Graph g) und LocallyHeaviestMatchingAlgorithmus(Graph g, int startknoten, int endknoten)
 * generiereMatching() und tryMatch(int knoten1, int knoten2, int gewicht): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben.
 * getIndex(int knoten): Suchen nach einer geeigneten Kante zu einem weiteren Knoten
 * getKante(): Suche nach einer geeigneten Kante, um die Berechnung durchfuehren zu koennen
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 * writeTable(): Tabelle des Matchings wird als einfacher String zurueckgegeben
 */
package ApproxAlgorithmen;

import java.util.ArrayList;

import GraphKlassen.Graph;
import GraphKlassen.Kante;

public class LocallyHeaviestMatchingAlgorithmus {
	private int anzahl_knoten;
	private Graph graph;
	private Graph matching;
	private ArrayList<Kante> kanten;
	private ArrayList<Kante> matchingKanten;
	private ArrayList<Kante> removedKanten;
	private boolean knotenGematched[];
	private int matchingGewicht = 0;
	private int startknoten;
	private int endknoten;
	public LocallyHeaviestMatchingAlgorithmus(Graph g) {
		this.graph = g;
		this.anzahl_knoten = g.getAnzahlKnoten();
		int[][] zwgraph = new int[anzahl_knoten][anzahl_knoten];
		for(int i = 0; i<anzahl_knoten; i++) {
			for(int j = 0; j < anzahl_knoten; j++) {
				zwgraph[i][j] = g.getGraph()[i][j];
			}
		}
		this.graph = new Graph(zwgraph, anzahl_knoten);
		this.kanten = new ArrayList<Kante>();
		for(int i = 0; i < g.getKantenliste().size(); i++) {
			this.kanten.add(g.getKantenliste().get(i));
		}
		matching = new Graph(anzahl_knoten);
		this.matchingKanten = new ArrayList<Kante>();
		this.removedKanten = new ArrayList<Kante>();
		knotenGematched = new boolean[anzahl_knoten];
		generiereMatching();
	}
	public LocallyHeaviestMatchingAlgorithmus(Graph g, int startknoten, int endknoten) {
		this.anzahl_knoten = g.getAnzahlKnoten();
		int[][] zwgraph = new int[anzahl_knoten][anzahl_knoten];
		for(int i = 0; i<anzahl_knoten; i++) {
			for(int j = 0; j < anzahl_knoten; j++) {
				zwgraph[i][j] = g.getGraph()[i][j];
			}
		}
		this.graph = new Graph(zwgraph, anzahl_knoten);
		this.kanten = new ArrayList<Kante>();
		for(int i = 0; i < g.getKantenliste().size(); i++) {
			this.kanten.add(g.getKantenliste().get(i));
		}
		matching = new Graph(anzahl_knoten);
		this.matchingKanten = new ArrayList<Kante>();
		this.removedKanten = new ArrayList<Kante>();
		knotenGematched = new boolean[anzahl_knoten];
		this.startknoten = startknoten+1;
		this.endknoten = endknoten+1;
		generiereMatching();
	}
	private void generiereMatching() {
		int[] kante = getKante();
		while(kante[0] != -1 && kante[1] != -1) {
			tryMatch(kante[0], kante[1],kante[2]);
			kante = getKante();
			for(int i = 0; i < this.anzahl_knoten; i++) {
				for(int j = i+1; j < this.anzahl_knoten; j++) {
				}
			}
		}
	}
	private int[] getKante() {
		int[] rueckgabe = {-1,-1,-1};
		for(int i = 0; i < this.anzahl_knoten; i++) {
			for(int j = i+1; j < this.anzahl_knoten; j++) {
				int gewicht = this.graph.getGraph()[i][j];
				if(gewicht > 0) {
					rueckgabe[0] = i;
					rueckgabe[1] = j;
					rueckgabe[2] = gewicht;
					return rueckgabe;
				}
					
			}
		}
		return rueckgabe;
	}

	public Graph getMatchingGraph() {
		return this.matching;
	}
	private void tryMatch(int knoten1, int knoten2, int gewicht) {
		ArrayList<Kante> kanten_knoten1 = new ArrayList<Kante>();
		ArrayList<Kante> kanten_knoten2 = new ArrayList<Kante>();
		int indexKanteKnoten1 = getIndex(knoten1);
		int indexKanteKnoten2 = getIndex(knoten2);
		while(!knotenGematched[knoten1] && !knotenGematched[knoten2] && (indexKanteKnoten1 != -1 || indexKanteKnoten2 != -1)) {
			System.out.println(knoten1 + "===" + knoten2);
			if(!knotenGematched[knoten1] && indexKanteKnoten1 != -1) {
				int gewichtTmp = this.graph.getGraph()[knoten1][indexKanteKnoten1];
				kanten_knoten1.add(new Kante(knoten1, indexKanteKnoten1, gewichtTmp));
				this.graph.updateKante(knoten1+1, indexKanteKnoten1+1, 0);
				this.graph.updateKante(indexKanteKnoten1+1, knoten1+1, 0);
				if(gewichtTmp > gewicht) {
					tryMatch(knoten1, indexKanteKnoten1, gewichtTmp);
				}
			}
			indexKanteKnoten2 = getIndex(knoten2);
			if(!knotenGematched[knoten2] && indexKanteKnoten2 != -1) {
				int gewichtTmp = this.graph.getGraph()[knoten2][indexKanteKnoten2];
				kanten_knoten2.add(new Kante(knoten2, indexKanteKnoten2, gewichtTmp));
				this.graph.updateKante(knoten2+1, indexKanteKnoten2+1, 0);
				this.graph.updateKante(indexKanteKnoten2+1, knoten2+1, 0);
				if(gewichtTmp > gewicht) {
					tryMatch(knoten2, indexKanteKnoten2, gewichtTmp);
				}
			}
			indexKanteKnoten1 = getIndex(knoten1);
			indexKanteKnoten2 = getIndex(knoten2);
		}
		if(knotenGematched[knoten1] && knotenGematched[knoten2]) {
			for(int i = 0; i < this.anzahl_knoten; i++) {
				this.graph.updateKante(knoten1+1, i+1, 0);
				this.graph.updateKante(i+1, knoten1+1, 0);
				this.graph.updateKante(knoten2+1, i+1, 0);
				this.graph.updateKante(i+1, knoten2+1, 0);
			}
		}
		else if(knotenGematched[knoten1] && !knotenGematched[knoten2]) {
			for(int i = 0; i < this.anzahl_knoten; i++) {
				this.graph.updateKante(knoten1+1, i+1, 0);
				this.graph.updateKante(i+1, knoten1+1, 0);
			}
			for(int i = 0; i < kanten_knoten2.size(); i++) {
				if(knoten2 == kanten_knoten2.get(i).getKnoten1() && knotenGematched[kanten_knoten2.get(i).getKnoten2()]) {
					this.graph.updateKante(kanten_knoten2.get(i).getKnoten1()+1, kanten_knoten2.get(i).getKnoten2()+1, 0);
					this.graph.updateKante(kanten_knoten2.get(i).getKnoten2()+1, kanten_knoten2.get(i).getKnoten1()+1, 0);
				}
				else if(knoten2 == kanten_knoten2.get(i).getKnoten2() && knotenGematched[kanten_knoten2.get(i).getKnoten1()]) {
					this.graph.updateKante(kanten_knoten2.get(i).getKnoten1()+1, kanten_knoten2.get(i).getKnoten2()+1, 0);
					this.graph.updateKante(kanten_knoten2.get(i).getKnoten2()+1, kanten_knoten2.get(i).getKnoten1()+1, 0);
				}
				else {
					this.graph.updateKante(kanten_knoten2.get(i).getKnoten1()+1, kanten_knoten2.get(i).getKnoten2()+1, kanten_knoten2.get(i).getGewicht());
					this.graph.updateKante(kanten_knoten2.get(i).getKnoten2()+1, kanten_knoten2.get(i).getKnoten1()+1, kanten_knoten2.get(i).getGewicht());
				}
			}
		}
		else if(!knotenGematched[knoten1] && knotenGematched[knoten2]) {
			for(int i = 0; i < this.anzahl_knoten; i++) {
				this.graph.updateKante(knoten2+1, i+1, 0);
				this.graph.updateKante(i+1, knoten2+1, 0);
			}
			for(int i = 0; i < kanten_knoten1.size(); i++) {
				if(knoten2 == kanten_knoten1.get(i).getKnoten1() && knotenGematched[kanten_knoten1.get(i).getKnoten2()]) {
					this.graph.updateKante(kanten_knoten1.get(i).getKnoten1()+1, kanten_knoten1.get(i).getKnoten2()+1, 0);
					this.graph.updateKante(kanten_knoten1.get(i).getKnoten2()+1, kanten_knoten1.get(i).getKnoten1()+1, 0);
				}
				else if(knoten2 == kanten_knoten1.get(i).getKnoten2() && knotenGematched[kanten_knoten1.get(i).getKnoten1()]) {
					this.graph.updateKante(kanten_knoten1.get(i).getKnoten1()+1, kanten_knoten1.get(i).getKnoten2()+1, 0);
					this.graph.updateKante(kanten_knoten1.get(i).getKnoten2()+1, kanten_knoten1.get(i).getKnoten1()+1, 0);
				}
				else {
					kanten.add(kanten_knoten1.get(i));
					this.graph.updateKante(kanten_knoten1.get(i).getKnoten1()+1, kanten_knoten1.get(i).getKnoten2()+1, kanten_knoten1.get(i).getGewicht());
					this.graph.updateKante(kanten_knoten1.get(i).getKnoten2()+1, kanten_knoten1.get(i).getKnoten1()+1, kanten_knoten1.get(i).getGewicht());
				}
			}
		}
		else {
			for(int i = 0; i < this.anzahl_knoten; i++) {
				this.graph.updateKante(knoten1+1, i+1, 0);
				this.graph.updateKante(i+1, knoten1+1, 0);
				this.graph.updateKante(knoten2+1, i+1, 0);
				this.graph.updateKante(i+1, knoten2+1, 0);
			}
			matchingKanten.add(new Kante(knoten1, knoten2, gewicht));
			matching.insertKante(knoten1+1, knoten2+1, gewicht);
			this.graph.updateKante(knoten1+1, knoten2+1, 0);
			this.graph.updateKante(knoten2+1, knoten1+1, 0);
			knotenGematched[knoten1] = true;
			knotenGematched[knoten2] = true;
		}
	}
	private int getIndex(int knoten) {
		int rueckgabe = -1;
		for(int i = 0; i < this.anzahl_knoten; i++) {
			if(this.graph.getGraph()[knoten][i] > 0) {
				return i;
			}
		}
		return rueckgabe;
	}
	public String writeTable() {
		String ausgabe="";
		int[][] m = matching.getGraph();
		for(int i = 0; i<anzahl_knoten; i++) {
			for(int j = 0; j<anzahl_knoten; j++) {
				ausgabe += m[i][j]+" ";
			}
			ausgabe+="\n";
		}
		return ausgabe;
	}
	public String writeTableHTML() {
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahl_knoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahl_knoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahl_knoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i)+"</tr>";
			this.matchingGewicht+= matchingKanten.get(i).getGewicht();
		}
		rueckgabe += "</table>";
		return rueckgabe;
	}
	public int getMatchingGewicht() {
		return matchingGewicht;
	}
}
