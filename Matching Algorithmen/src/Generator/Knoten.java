package Generator;

public class Knoten {
	private int x;
	private int y;
	public Knoten(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return this.x;
	}
	public int getY() {
		return this.y;
	}
}
