package Generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import GraphKlassen.Graph;

public class Generator2 {
	private FileReader fr;
	private Graph g;
	private int anzahlKnoten;
	private Knoten[] knoten;
	private int max_x = 0;
	private int min_x = Integer.MAX_VALUE;
	public Generator2(File file) throws IOException{
		this.fr = new FileReader(file);
	}
	public void berechneGraph() {
		try {
			BufferedReader br = new BufferedReader(fr);
			this.anzahlKnoten = Integer.parseInt(br.readLine());
			this.g = new Graph(this.anzahlKnoten);
			String zeile;
			int durchlauf = 0;
			knoten = new Knoten[anzahlKnoten];
			while((zeile = br.readLine()) != null){
				String[] split = zeile.split(" ");
				int x = Integer.parseInt(split[0]);
				int y = Integer.parseInt(split[1]);
				if(x > max_x)
					max_x = x;
				if(x < min_x)
					min_x = x;
				knoten[durchlauf] = new Knoten(x,y);
				durchlauf++;
			}
			br.close();
			for(int i = 0; i < this.anzahlKnoten; i++) {
				for(int j = i+1; j < this.anzahlKnoten; j++) {
					if(knoten[i].getX() > knoten[j].getX() && knoten[i].getY() > knoten[j].getY()) {
						int xwert = knoten[i].getX() * knoten[j].getX();
						int ywert = knoten[i].getY() * knoten[j].getY();
						int addition = xwert+ywert;
						double gewicht = Math.sqrt(addition);
						int ganzGewicht = (int)gewicht;
						if(ganzGewicht != 0 && ganzGewicht > ((max_x - min_x)/1.5)) {
							this.g.insertKante(i+1, j+1, ganzGewicht);
						}
						
					}
				}
			}
		}catch(IOException e) {
			System.out.println("Ein Fehler beim Laden der Datei ist aufgetreten");
		}
	}
	public Graph getGraph() {
		return g;
	}
}
