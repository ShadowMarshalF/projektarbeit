package Generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import GraphKlassen.Graph;

public class Generator {
	private FileReader fr;
	private Graph g;
	private int anzahlKnoten;
	private Knoten[] knoten;
	public Generator(File file) throws IOException{
		this.fr = new FileReader(file);
	}
	public void berechneGraph() {
		try {
			
			BufferedReader br = new BufferedReader(fr);
			this.anzahlKnoten = Integer.parseInt(br.readLine());
			this.g = new Graph(this.anzahlKnoten);
			String zeile;
			int durchlauf = 0;
			knoten = new Knoten[anzahlKnoten];
			while((zeile = br.readLine()) != null){
				String[] split = zeile.split(" ");
				double x1 = Double.parseDouble(split[1]);
				int x = (int) x1;
				double y1 = Double.parseDouble(split[2]);
				int y = (int) y1;
				knoten[durchlauf] = new Knoten(x,y);
				durchlauf++;
			}
			br.close();
			for(int i = 0; i < this.anzahlKnoten; i++) {
				for(int j = i+1; j < this.anzahlKnoten; j++) {
					int xwert = knoten[i].getX() * knoten[j].getX();
					int ywert = knoten[i].getY() * knoten[j].getY();
					int addition = xwert+ywert;
					double gewicht = Math.sqrt(addition);
					int ganzGewicht = (int)gewicht;
					if(ganzGewicht == 0) {
						ganzGewicht = 1;
					}
					this.g.insertKante(i+1, j+1, ganzGewicht);
					
				}
				double prozent = (i*100/this.anzahlKnoten);
				System.out.println(prozent + "%");
			}
		}catch(IOException e) {
			System.out.println("Ein Fehler beim Laden der Datei ist aufgetreten");
		}
	}
	public Graph getGraph() {
		return g;
	}
}
