package EdmondsBlossomAlgorithmus;

public class Pfad {
	private int[] Nachfolger;
	private int ersterKnoten;
	private int letztenKnoten;
	private int anzahlknoten;
	public Pfad(int anzahlKnoten, int ersterKnoten) {
		this.anzahlknoten = anzahlKnoten;
		this.Nachfolger = new int[anzahlKnoten];
		this.ersterKnoten = ersterKnoten;
		this.letztenKnoten = ersterKnoten;
		for(int i = 0; i < anzahlKnoten; i++)
			this.Nachfolger[i] = -1;
	}
	public void setNachfolger(int Knoten, int NachfolgeKnoten) {
		this.Nachfolger[Knoten] = NachfolgeKnoten;
		this.letztenKnoten = NachfolgeKnoten;
	}
	public int getNachfolger(int Knoten) {
		return this.Nachfolger[Knoten];
	}
	public int getErstenKnoten() {
		return this.ersterKnoten;
	}
	public int getLetztenKnoten() {
		return this.letztenKnoten;
	}
	public String toString() {
		String rueckgabe = "";
		int knoten = ersterKnoten; 
		while(knoten != -1) {
			rueckgabe += knoten;
			knoten = getNachfolger(knoten);
			if(knoten != -1)
				rueckgabe +=  " -> ";
		}
		return rueckgabe;
	}
	public int getLaenge() {
		int laenge = 0;
		int knoten = getErstenKnoten();
		while(knoten != -1) {
			laenge += 1;
			knoten = getNachfolger(knoten);
		}
		return laenge-1;
	}
	public Pfad pfadumkehren(Pfad pfad, int knoten) {
		int nachfolger = pfad.getNachfolger(knoten);
		Pfad rueckgabe;
		if(nachfolger == -1) {
			rueckgabe = new Pfad(pfad.anzahlknoten, pfad.getLetztenKnoten());
		}
		else {
			rueckgabe = pfadumkehren(pfad, nachfolger);
			rueckgabe.setNachfolger(nachfolger, knoten);
		}
		return rueckgabe;
	}
}
