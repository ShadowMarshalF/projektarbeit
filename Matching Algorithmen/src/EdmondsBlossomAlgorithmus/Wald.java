package EdmondsBlossomAlgorithmus;

import java.util.ArrayList;

public class Wald {
	private ArrayList<Integer> wurzelknoten;
	private WaldElementInformationen[] nachfolger;
	private boolean[] knotenbehandelt;
	private boolean[] knotenvorhanden;
	private int anzahlknoten;
	public Wald(int anzahlknoten) {
		this.wurzelknoten = new ArrayList<Integer>();
		this.nachfolger = new WaldElementInformationen[anzahlknoten];
		this.knotenbehandelt = new boolean[anzahlknoten];
		this.knotenvorhanden = new boolean[anzahlknoten];
		this.anzahlknoten = anzahlknoten;
		for(int i = 0; i < nachfolger.length; i++) {
			nachfolger[i] = new WaldElementInformationen();
		}
	}
	public void setWurzelKnoten(int wurzelknoten) {
		this.wurzelknoten.add(wurzelknoten);
		this.nachfolger[wurzelknoten].setWurzelknoten(wurzelknoten);
		knotenvorhanden[wurzelknoten] = true;
	}
	public void setBetrachtungsknoten(int wurzelknoten) {
		this.wurzelknoten.add(0, wurzelknoten);
	}
	public int getWurzelknoten() {
		if(this.wurzelknoten.size() == 0)
			return -1;
		int rueck = this.wurzelknoten.get(0);
		this.wurzelknoten.remove(0);
		return rueck;
	}
	public int getAnzahlWurzelknoten() {
		return this.wurzelknoten.size();
	}
	public WaldElementInformationen getElementInformation(int knoten) {
		return this.nachfolger[knoten];
	}
	public void setNachfolger(int knoten, int nachfolgeknoten) {
		nachfolger[knoten].setNachfolger(nachfolgeknoten);
		nachfolger[nachfolgeknoten].setWurzelknoten(nachfolger[knoten].getWurzelknoten());
		knotenvorhanden[nachfolgeknoten] = true;
	}
	public int getAnzahlknoten() {
		return this.anzahlknoten;
	}
	public String toString() {
		String rueckgabe = "";
		
		for(int i = 0; i < this.wurzelknoten.size(); i++)
			rueckgabe += (i+1) + ". Wurzelknoten: " + (this.wurzelknoten.get(i)+1) + " ";
		rueckgabe += "\n";
		for(int i = 0; i < this.nachfolger.length; i++) {
			rueckgabe += (i+1) + ".Knoten hat folgende Nachfolger: \n";
			rueckgabe += this.nachfolger[i];
			rueckgabe += "\n";
		}
		
		return rueckgabe;
	}
	public boolean getknotenvorhanden(int knoten) {
		return this.knotenvorhanden[knoten];
	}
	public boolean getknotenbehandelt(int knoten) {
		return this.knotenbehandelt[knoten];
	}
	public void setknotenbehandelt(int knoten) {
		this.knotenbehandelt[knoten] = true;
	}
	public boolean istWurzelknoten(int knoten) {
		boolean rueckgabe = false;
		for(int i = 0; i < this.wurzelknoten.size(); i++) {
			if(this.wurzelknoten.get(i) == knoten)
				rueckgabe = true;
		}
		return rueckgabe;
	}
	public boolean istNachfolger(int knoten1, int knoten2) {
		boolean rueckgabe = false;
		for(int i = 0; i < nachfolger[knoten1].getAnzahlNachfolger(); i++) {
			if(nachfolger[knoten1].getNachfolger(i) == knoten2)
				rueckgabe = true;
		}
		
		return rueckgabe;
	}
}
