package EdmondsBlossomAlgorithmus;

import java.util.ArrayList;

public class WaldElementInformationen {
	private ArrayList<Integer> nachfolger;
	private int wurzelknoten;
	public WaldElementInformationen() {
		this.nachfolger = new ArrayList<Integer>();
		this.wurzelknoten = -1;
	}
	public void setNachfolger(int nachfolgeknoten) {
		this.nachfolger.add(nachfolgeknoten);
	}
	public int getNachfolger(int id) {
		return this.nachfolger.get(id);
	}
	public int getAnzahlNachfolger() {
		return this.nachfolger.size();
	}
	public void setWurzelknoten(int wurzelknoten) {
		this.wurzelknoten = wurzelknoten;
	}
	public int getWurzelknoten() {
		return this.wurzelknoten;
	}
	public String toString() {
		String rueckgabe = "";
		for(int i = 0; i < this.nachfolger.size(); i++)
			rueckgabe += (i+1) + ". Nachfolger: " + (this.nachfolger.get(i)+1) + "\n";
		rueckgabe += " Wurzelknoten: " + (this.wurzelknoten+1) + "\n";
		return rueckgabe;
	}
}
