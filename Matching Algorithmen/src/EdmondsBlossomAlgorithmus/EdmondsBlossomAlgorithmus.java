/* Edmonds Blossom Algorithmus
 * Bietet den Konstruktor EdmondsBlossomAlgorithmus(int[][] graph, int[][] matching, int anzahlKnoten)
 * berechneMatching(), sucheAugmentierendenPfad(), zumWaldhinzu(Wald F, int knoten1, int knoten2, int knoten3),
 * 				berechneaugPfad(Wald wald, int knoten1, int knoten2) und berechneKreis(Wald wald, int knoten1, int knoten2): 
 * 								Ist die eigentliche Berechnung des Algorithmus, wie in der Seminararbeit beschrieben.
 * berechnePfad(Pfad pfad, Wald wald, int knoten1, int knoten2): Der eigentliche Pfad wird erstellt, und das Matching dahingehend angepasst
 * getErgebnis(): Rueckgabe, ob und wie haeufig erweitert werden konnte 
 */
package EdmondsBlossomAlgorithmus;

import GraphKlassen.Graph;

public class EdmondsBlossomAlgorithmus {
	private int[][] graph;
	private int[][] matching;
	private int[][] marked;
	private int anzahlknoten;
	private int anzahlVeraenderungen;
	public EdmondsBlossomAlgorithmus(int[][] graph, int[][] matching, int anzahlKnoten) {
		this.anzahlknoten = anzahlKnoten;
		this.anzahlVeraenderungen = 0;
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i < anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = graph[i][j];
			}
		}
		this.matching = new int[anzahlKnoten][anzahlKnoten];
		this.marked = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i < anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.matching[i][j] = matching[i][j];
				this.marked[i][j] = matching[i][j];
			}
		}
	}
	public Graph berechneMatching() { // Algorithm1
		Graph rueckgabe = new Graph(this.matching, this.anzahlknoten);
		Pfad pfad = sucheAugmentierendenPfad();
		if(pfad != null) {
			int ersterknoten = pfad.getErstenKnoten();
			int nachsterknoten = pfad.getNachfolger(ersterknoten);
			for(int i = 0; i < pfad.getLaenge() && nachsterknoten != -1; i++) {
				if(i % 2 == 0) {
					rueckgabe.updateKante(ersterknoten+1, nachsterknoten+1, 1);
				}
				else if(i % 2 == 1) {
					rueckgabe.updateKante(ersterknoten+1, nachsterknoten+1, 0);
				}
				ersterknoten = nachsterknoten;
				nachsterknoten = pfad.getNachfolger(ersterknoten);
			}
			this.matching = rueckgabe.getGraph();
			this.marked = rueckgabe.getGraph();
			rueckgabe = berechneMatching();
		}
		else {
		}
		return rueckgabe;
	}
	
	private Pfad sucheAugmentierendenPfad() { //Algorithm2
		Pfad pfad = null;
		Wald F = new Wald(this.anzahlknoten);
		for(int i = 0; i < this.anzahlknoten; i++) {
			boolean frei = true;
			for(int j = 0; j < this.anzahlknoten; j++) {
				if(matching[i][j] > 0) {
					frei = false;
				}
			}
			if(frei)
				F.setWurzelKnoten(i);
		}
		int aktuellerKnoten;
		while((aktuellerKnoten = F.getWurzelknoten()) != -1) {
			F.setknotenbehandelt(aktuellerKnoten);
			for(int w = 0; w < this.anzahlknoten; w++) {
				if(graph[aktuellerKnoten][w] > 0) {
					for(int x = 0; x < this.anzahlknoten; x++) {
						if(matching[w][x] > 0 && marked[aktuellerKnoten][w] == 0) {
							if(!F.getknotenvorhanden(w)) {
								F = zumWaldhinzu(F, aktuellerKnoten, w, x);
							}
							else {
								Pfad test = berechnePfad(new Pfad(this.anzahlknoten, F.getElementInformation(w).getWurzelknoten()), 
										F, F.getElementInformation(w).getWurzelknoten(), w);
								if(test.getLaenge()%2 == 0) {
									
									if(F.getElementInformation(w).getWurzelknoten() != F.getElementInformation(aktuellerKnoten).getWurzelknoten()) {
										Pfad rueckgabe = berechneaugPfad(F, aktuellerKnoten, w);
										if(rueckgabe != null)
											return rueckgabe;
										
									}
									else {
										Pfad rueckgabe = berechneKreis(F, aktuellerKnoten, w);
										if(rueckgabe != null) {
											return rueckgabe;
										}
									}
									
								}
							}
							marked[aktuellerKnoten][w] = 1;
							marked[w][aktuellerKnoten] = 1;
						}
					}
				}
			}
		}
		//System.out.println(F);
		return pfad;
	}
	
	private Wald zumWaldhinzu(Wald F, int knoten1, int knoten2, int knoten3) { //Algorithm3
		Wald rueckgabe = F;
		F.setNachfolger(knoten1, knoten2);
		F.setNachfolger(knoten2, knoten3);
		F.setBetrachtungsknoten(knoten2);
		F.setBetrachtungsknoten(knoten3);
		return rueckgabe;
	}
	
	private Pfad berechneaugPfad(Wald wald, int knoten1, int knoten2) { //Algorithm4
		Pfad rueckgabe = null;
		int ntrk1 = wald.getElementInformation(knoten1).getWurzelknoten();
		int ntrk2 = wald.getElementInformation(knoten2).getWurzelknoten();
		Pfad pfad1 = berechnePfad(new Pfad(this.anzahlknoten, ntrk1), wald, ntrk1, knoten1);
		Pfad pfad2 = berechnePfad(new Pfad(this.anzahlknoten, ntrk2), wald, ntrk2, knoten2);
		pfad2 = pfad2.pfadumkehren(pfad2, pfad2.getErstenKnoten());
		if(graph[pfad1.getLetztenKnoten()][pfad2.getErstenKnoten()] > 0) {
			rueckgabe = pfad1;
			rueckgabe.setNachfolger(rueckgabe.getLetztenKnoten(), pfad2.getErstenKnoten());
			int nachfolger = pfad2.getErstenKnoten();
			for(int i = 0; i < pfad2.getLaenge(); i++) {
				rueckgabe.setNachfolger(rueckgabe.getLetztenKnoten(), pfad2.getNachfolger(nachfolger));
				nachfolger = pfad2.getNachfolger(nachfolger);
			}
			this.anzahlVeraenderungen += 1;
		}
		return rueckgabe;
	}
	
	private Pfad berechneKreis(Wald wald, int knoten1, int knoten2) { // Algorithm5
		int neugraph[][];
		int neumatching[][];
		Pfad pfad1 = berechnePfad(new Pfad(this.anzahlknoten, wald.getElementInformation(knoten1).getWurzelknoten()), wald, wald.getElementInformation(knoten1).getWurzelknoten(), knoten1);
		Pfad pfad2 = berechnePfad(new Pfad(this.anzahlknoten, wald.getElementInformation(knoten1).getWurzelknoten()), wald, wald.getElementInformation(knoten1).getWurzelknoten(), knoten2);
		pfad2 = pfad2.pfadumkehren(pfad2, pfad2.getErstenKnoten());
		Pfad rueckgabe = new Pfad(this.anzahlknoten, pfad1.getErstenKnoten());
		int nachfolger = pfad1.getErstenKnoten(); 
		rueckgabe.setNachfolger(rueckgabe.getErstenKnoten(), pfad1.getErstenKnoten());
		for(int i = 0; i < pfad1.getLaenge(); i++) {
			rueckgabe.setNachfolger(rueckgabe.getLetztenKnoten(), pfad1.getNachfolger(nachfolger));
			nachfolger = pfad1.getNachfolger(nachfolger);
		}
		if(graph[pfad1.getErstenKnoten()][pfad2.getLetztenKnoten()] > 0) {
			rueckgabe.setNachfolger(rueckgabe.getLetztenKnoten(), pfad2.getErstenKnoten());
			nachfolger = pfad2.getErstenKnoten();
			for(int i = 0; i < pfad2.getLaenge()-1; i++) {
				rueckgabe.setNachfolger(rueckgabe.getLetztenKnoten(), pfad2.getNachfolger(nachfolger));
				nachfolger = pfad2.getNachfolger(nachfolger);
			}
			boolean[] imkreis = new boolean[this.anzahlknoten];
			nachfolger = rueckgabe.getErstenKnoten();
			int kleinsterWert = nachfolger;
			imkreis[nachfolger] = true;
			for(int i = 0; i < rueckgabe.getLaenge(); i++) {
				imkreis[rueckgabe.getNachfolger(nachfolger)] = true;
				nachfolger = rueckgabe.getNachfolger(nachfolger);
				if(kleinsterWert > nachfolger)
					kleinsterWert = nachfolger;
			}
			int ineugraph = 0;
			neugraph = new int[this.anzahlknoten-rueckgabe.getLaenge()][this.anzahlknoten-rueckgabe.getLaenge()];
			neumatching = new int[this.anzahlknoten-rueckgabe.getLaenge()][this.anzahlknoten-rueckgabe.getLaenge()];
			for(int i = 0; i < this.anzahlknoten; i++) {
				int jneugraph = 0;
				for(int j = 0; j < this.anzahlknoten; j++) {
					if(imkreis[i] && !imkreis[j] && this.graph[i][j] > 0) {
						neugraph[kleinsterWert][jneugraph] = this.graph[i][j];
						neumatching[kleinsterWert][jneugraph] = this.matching[i][j];
					}
					else if(!imkreis[i] && imkreis[j] && this.graph[i][j] > 0) {
						neugraph[ineugraph][kleinsterWert] = this.graph[i][j];
						neumatching[ineugraph][kleinsterWert] = this.matching[i][j];
					}
					else if(!imkreis[i] && !imkreis[j]) {
						neugraph[ineugraph][jneugraph] = this.graph[i][j];
						neumatching[ineugraph][jneugraph] = this.matching[i][j];
						jneugraph++;
						if(jneugraph == 2)
							jneugraph++;
					}
				}
				if(!imkreis[i])
					ineugraph++;
				if(ineugraph == 2)
					ineugraph++;
				jneugraph = 0;
			}
			Pfad ergebnis = new EdmondsBlossomAlgorithmus(neugraph, neumatching, this.anzahlknoten-rueckgabe.getLaenge()).sucheAugmentierendenPfad();
			boolean drin = false;
			if(ergebnis != null) {
				nachfolger = ergebnis.getErstenKnoten();
				if(nachfolger == kleinsterWert)
					drin = true;
				for(int i = 0; i < ergebnis.getLaenge(); i++) {
					if(ergebnis.getNachfolger(nachfolger) == knoten2)
						drin = true;
					nachfolger = ergebnis.getNachfolger(nachfolger);
				}
				if(drin) {
					System.out.println("OHOH");
					//Muss noch gemacht werden
				}
				else {
					rueckgabe = ergebnis;
				}
			}
			else {
				rueckgabe = ergebnis;
			}
		}
		else {
			rueckgabe = null;
		}
		return rueckgabe;
	}
	
	//#####################################Hilfsmethoden#####################################

	private Pfad berechnePfad(Pfad pfad, Wald wald, int knoten1, int knoten2) {
		Pfad rueckgabe = pfad;
		for(int i = 0; i < wald.getElementInformation(knoten1).getAnzahlNachfolger(); i++) {
			int nachfolger = wald.getElementInformation(knoten1).getNachfolger(i);
			rueckgabe.setNachfolger(knoten1, nachfolger);
			rueckgabe = berechnePfad(rueckgabe, wald, nachfolger, knoten2);
			if(rueckgabe.getLetztenKnoten() == knoten2)
				return rueckgabe;
		}
		
		
		return rueckgabe;
	}
	public String getErgebnis() {
		if(this.anzahlVeraenderungen == 0)
			return "Das Matching konnte nicht verbessert werden!";
		else
			return "Das Matching wurde um die Länge " + this.anzahlVeraenderungen + " verbessert!";
	}
}
