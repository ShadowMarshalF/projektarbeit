/* Disjoined Path Growing Algorithmus
 * Bietet die zwei verschiedenen Konstruktoren DisjoinedPathGrowing(Graph g) und DisjoinedPathGrowing(Graph g, int startknoten)
 * berechneMatching(): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben.
 * getFreienKnoten(): Suche beim Starten eines neuen Pfades nach einem neuen Startknoten
 * berechneMatchingFakten(): Die MatchingBelegungen werden nacheinander durchgegeangen und zu einem Matching zusammengefuegt
 * korrektesMatching(): Es wird ueberprueft, ob der Matching-Graph fuer jeden Knoten nur eine Kante zu einem anderen Knoten besitzt und wahr oder falsch zurueckgegeben
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 * testeStartknoten(): Ueberpruefung aller Startknotens, ob eine Verbindung zwischen diesen moeglich ist, um ein maximales Matching zu erhalten.
 */
package Analyse;
import java.util.ArrayList;
import GraphKlassen.*;
public class DisjoinedPathGrowing {
	private int anzahlKnoten;
	private int matchingWert = 0;
	private int anzahlPfade = 0;
	private int[][] graph;
	private int[][] matchingGraph;
	private ArrayList<Integer> startknoten;
	private ArrayList<MatchingBelegung> matchingBelegung;
	private ArrayList<Kante> matching;
	private boolean knotenVerfuegbar[];
	private boolean knotenVerfuegbarSK[];
	private int matchingWahl = 1;
	public DisjoinedPathGrowing(Graph g) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		this.matchingGraph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingBelegung = new ArrayList<MatchingBelegung>();
		this.matching = new ArrayList<Kante>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		knotenVerfuegbarSK = new boolean[anzahlKnoten];
		this.startknoten = new ArrayList<Integer>();
		int knoten;
		while((knoten = getFreienKnoten()) != -1) {
			this.matchingWahl = 1;
			this.startknoten.add(knoten);
			this.matchingBelegung.add(new MatchingBelegung(knoten));
			berechneMatching(knoten);		
			anzahlPfade++;
		}
		berechneMatchingFakten();
		testeStartknoten();
	}
	public DisjoinedPathGrowing(Graph g, int startknoten) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		this.matchingGraph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingBelegung = new ArrayList<MatchingBelegung>();
		this.matching = new ArrayList<Kante>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		knotenVerfuegbarSK = new boolean[anzahlKnoten];
		int knoten;
		this.matchingBelegung.add(new MatchingBelegung(startknoten));
		this.startknoten = new ArrayList<Integer>();
		this.startknoten.add(startknoten);
		berechneMatching(startknoten);
		anzahlPfade++;
		while((knoten = getFreienKnoten()) != -1) {
			this.matchingWahl = 1;
			this.startknoten.add(knoten);
			this.matchingBelegung.add(new MatchingBelegung(knoten));
			berechneMatching(knoten);
			anzahlPfade++;	
		}
		berechneMatchingFakten();
		testeStartknoten();
	}
	private void berechneMatching(int startKnoten) {
			int tmp = 0;
			int endKnoten = -1;
			for(int i = 0; i < anzahlKnoten; i++) {
				if(graph[startKnoten][i]>tmp && !knotenVerfuegbar[i]) {
					tmp = graph[startKnoten][i];
					endKnoten = i;
				}
			}
			knotenVerfuegbar[startKnoten] = true;
			if(endKnoten != -1) {	
				if(this.matchingWahl == 1) {
					matchingBelegung.get(anzahlPfade).insertM1(new Kante(startKnoten+1,endKnoten+1,tmp));
					matchingGraph[startKnoten][endKnoten] = tmp;
					matchingGraph[endKnoten][startKnoten] = tmp;
				}
				else {
					matchingBelegung.get(anzahlPfade).insertM2(new Kante(startKnoten+1,endKnoten+1,tmp));
					matchingGraph[startKnoten][endKnoten] = tmp;
					matchingGraph[endKnoten][startKnoten] = tmp;
				}
				this.matchingWahl = 3 - this.matchingWahl;
				berechneMatching(endKnoten);
			}
	
	}
	private int getFreienKnoten() {
		int rueckgabe = -1;
		for(int i = 0; i < anzahlKnoten;i++)
			if(!knotenVerfuegbar[i]) {
				rueckgabe = i;
				break;
			}
		return rueckgabe;
	}
	private void berechneMatchingFakten() {
		for(int i = 0; i < anzahlPfade; i++) {
			ArrayList<Kante> zwspeicher = matchingBelegung.get(i).getBesteBelegung();
			for(int j = 0; j < zwspeicher.size(); j++) {
				this.matching.add(zwspeicher.get(j));
				this.knotenVerfuegbarSK[zwspeicher.get(j).getKnoten1()-1] = true;
				this.knotenVerfuegbarSK[zwspeicher.get(j).getKnoten2()-1] = true;
				this.matchingWert += zwspeicher.get(j).getGewicht();
			}
		}
	}
	public boolean korrektesMatching() {
		Graph graphMatching = getMatchingGraph();
		boolean rueckgabe = true;
		System.out.println("###########");
		for(int i = 0; i < this.anzahlKnoten; i++) {
			int anzahl = 0;
			
			for(int j = 0; j < this.anzahlKnoten; j++) {
				if(graphMatching.getGraph()[i][j] > 0) {
					anzahl++;
					if(anzahl > 1)
						System.out.println(i + "i " + j + "j");
				}
			}
			if(anzahl > 1) {
				rueckgabe = false;
			}
		}
		return rueckgabe;
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe = new Graph(this.anzahlKnoten);
		for(int  i = 0; i < this.matching.size(); i++) {
			rueckgabe.insertKante(this.matching.get(i).getKnoten1(), this.matching.get(i).getKnoten2(), this.matching.get(i).getGewicht());
		}
		return rueckgabe;
	}	
	public int getMatchingGewicht() {
		return this.matchingWert;
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		for(int i = 0; i < matching.size(); i++) {
			rueckgabe += "<tr>" + matching.get(i)+"</tr>";
		}
		rueckgabe += "</table>";
		return rueckgabe;
	}
	public String writeTableHTML() {
		Graph graphMatching = new Graph(anzahlKnoten);
		for(int i = 0; i < matching.size();i++) {
			graphMatching.insertKante(matching.get(i).getKnoten1(), matching.get(i).getKnoten2(), matching.get(i).getGewicht());
		}
		int[][] m = graphMatching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
	private void testeStartknoten() {
		for(int i = 0; i < this.startknoten.size(); i++) {
			for(int j = i+1; j < this.startknoten.size(); j++) {
				int wertK1 = this.startknoten.get(i);
				int wertK2 = this.startknoten.get(j);
				if(this.knotenVerfuegbarSK[wertK1] == false && this.knotenVerfuegbarSK[wertK2] == false && this.graph[wertK1][wertK2] > 0) {
					this.matching.add(new Kante(wertK1+1,wertK2+1,this.graph[wertK1][wertK2]));
					this.matchingWert += this.graph[wertK1][wertK2];
					this.knotenVerfuegbarSK[wertK1] = true;
					this.knotenVerfuegbarSK[wertK2] = true;
					this.startknoten.remove(i);
					this.startknoten.remove(j-1);
				}
			}
		}
	}
}
