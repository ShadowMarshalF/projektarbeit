/* Expanded Path Growing Algorithmus
 * Bietet die zwei verschiedenen Konstruktoren ExpandedPathGrowingAlgorithmus(Graph g) und ExpandedPathGrowingAlgorithmus(Graph g, int startknoten)
 * berechneMatching(int startknoten): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben. Übergabeparameter ist der startknoten.
 * getFreienKnoten(): Suche beim Starten eines neuen Pfades nach einem neuen Startknoten
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 * getMatching1() und getMatching2(): Jeweils der Wert der Belegung wird zurueckgegeben
 * expandMatching(): Es wird ueberprueft, ob ein Startknoten mit einem Endknoten verbunden werden kann.
 */
package Analyse;
import java.util.ArrayList;
import GraphKlassen.*;
public class ExpandedPathGrowingAlgorithmus {
	private int anzahlKnoten;
	private int matchingGewicht1 = 0;
	private int matchingGewicht2 = 0;
	private int[][] graph;
	private ArrayList<Kante> matchingKanten;
	private ArrayList<Kante> matchingKanten2;
	private ArrayList<Integer> startknoten;
	private ArrayList<Integer> endknoten;
	private boolean knotenVerfuegbar[];
	private int matchingWahl = 1;
	public ExpandedPathGrowingAlgorithmus(Graph g) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten = new ArrayList<Kante>();
		this.matchingKanten2 = new ArrayList<Kante>();
		this.startknoten = new ArrayList<Integer>();
		this.endknoten = new ArrayList<Integer>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		int knoten;
		while((knoten = getFreienKnoten()) != -1) {
			this.startknoten.add(knoten);
			berechneMatching(knoten);
		}
		expandMatching();
	}
	public ExpandedPathGrowingAlgorithmus(Graph g, int startknoten) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten = new ArrayList<Kante>();
		this.matchingKanten2 = new ArrayList<Kante>();
		this.startknoten = new ArrayList<Integer>();
		this.endknoten = new ArrayList<Integer>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		int knoten;
		this.startknoten.add(startknoten);
		berechneMatching(startknoten);
		while((knoten = getFreienKnoten()) != -1) {
			this.startknoten.add(knoten);
			berechneMatching(knoten);
		}
		expandMatching();
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe = new Graph(this.anzahlKnoten);
		Graph rueckgabe2 = new Graph(this.anzahlKnoten);
		for(int i = 0; i < this.matchingKanten.size(); i++) {
			rueckgabe.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		for(int i = 0; i < this.matchingKanten2.size(); i++) {
			rueckgabe2.insertKante(matchingKanten2.get(i).getKnoten1(), matchingKanten2.get(i).getKnoten2(), matchingKanten2.get(i).getGewicht());
		}
		if(matchingGewicht1 > matchingGewicht2)	
			return rueckgabe;
		else
			return rueckgabe2;
	}
	private void berechneMatching(int startKnoten) {
			int tmp = 0;
			int endKnoten = -1;
			for(int i = 0; i < anzahlKnoten; i++) {
				if(graph[startKnoten][i]>tmp && !knotenVerfuegbar[i]) {
					tmp = graph[startKnoten][i];
					endKnoten = i;
				}
			}
			knotenVerfuegbar[startKnoten] = true;
			if(endKnoten != -1) {	
				if(matchingWahl == 1) 
					matchingKanten.add(new Kante(startKnoten+1,endKnoten+1,tmp));
				else
					matchingKanten2.add(new Kante(startKnoten+1,endKnoten+1,tmp));
				matchingWahl = 3 - matchingWahl;
				berechneMatching(endKnoten);
			}
			else if(matchingWahl == 2){
				this.endknoten.add(startKnoten);
			}
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		String rueckgabe2="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";;
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i)+"</tr>";
			this.matchingGewicht1+= matchingKanten.get(i).getGewicht();
		}
		rueckgabe += "</table>";
		for(int i = 0; i < matchingKanten2.size(); i++) {
			rueckgabe2 += "<tr>" + matchingKanten2.get(i)+"</tr>";
			this.matchingGewicht2+= matchingKanten2.get(i).getGewicht();
		}
		rueckgabe2 += "</table>";
		if(matchingGewicht1 > matchingGewicht2)	
			return rueckgabe;
		else
			return rueckgabe2;
	}
	public int getMatchingGewicht() {
		if(matchingGewicht1 > matchingGewicht2)	
			return matchingGewicht1;
		else
			return matchingGewicht2;
	}
	private int getFreienKnoten() {
		int rueckgabe = -1;
		for(int i = 0; i < anzahlKnoten;i++)
			if(!knotenVerfuegbar[i]) {
				rueckgabe = i;
				break;
			}
		return rueckgabe;
	}
	public int getMatching1() {
		return this.matchingGewicht1;
	}
	public int getMatching2() {
		return this.matchingGewicht2;
	}
	public String writeTableHTML() {
		Graph matching = new Graph(anzahlKnoten);
		if(matchingGewicht1 > matchingGewicht2) {
			for(int i = 0; i < matchingKanten.size();i++)
				matching.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		else{
			for(int i = 0; i < matchingKanten2.size();i++)
				matching.insertKante(matchingKanten2.get(i).getKnoten1(), matchingKanten2.get(i).getKnoten2(), matchingKanten2.get(i).getGewicht());
		}
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
	private void expandMatching() {
			for(int i = 0; i < this.endknoten.size(); i++) {
				int tmpgewicht = 0;
				int knoten1 = -1;
				int knoten2 = -1;
				int deleteSK = -1;
				int deleteEK = -1;
				for(int j = 0; j < this.startknoten.size(); j++) {
					if(graph[this.endknoten.get(i)][this.startknoten.get(j)] > tmpgewicht) {
						knoten1 = this.startknoten.get(j);
						knoten2 = this.endknoten.get(i);
						tmpgewicht = graph[knoten1][knoten2];
						deleteSK = j;
						deleteEK = i;
					}
				}
				boolean vorhanden = false;
				for(int j = 0; j < this.matchingKanten.size(); j++) {
					if((knoten1 == this.matchingKanten.get(j).getKnoten1()-1 && knoten2 == this.matchingKanten.get(j).getKnoten2()-1) ||
						(knoten2 == this.matchingKanten.get(j).getKnoten2()-1 && knoten1 == this.matchingKanten.get(j).getKnoten1()-1))
						vorhanden = true;
				}
				if(deleteSK != -1 && deleteEK != -1 && !vorhanden) {
					matchingKanten2.add(new Kante(knoten1+1,knoten2+1,tmpgewicht));
					this.startknoten.remove(deleteSK);
					this.endknoten.remove(deleteEK);
				}
			}
	}
}
