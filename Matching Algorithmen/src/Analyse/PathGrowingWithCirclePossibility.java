/* Path Growing With Circle Possibility Algorithmus
 * Bietet die zwei verschiedenen Konstruktoren PathGrowingWithCirclePossibility(Graph g) und PathGrowingWithCirclePossibility(Graph g, int startknoten)
 * berechneMatching(int startknoten): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben. Übergabeparameter ist der startknoten.
 * getFreienKnoten(): Suche beim Starten eines neuen Pfades nach einem neuen Startknoten
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * getMatching1() und getMatching2(): Jeweils der Wert der Belegung wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 */
package Analyse;
import java.util.ArrayList;
import GraphKlassen.*;
public class PathGrowingWithCirclePossibility {
	private int anzahlKnoten;
	private int matchingGewicht1 = 0;
	private int matchingGewicht2 = 0;
	private int[][] graph;
	private int[][] matchingGraph;
	private ArrayList<Kante> matchingKanten;
	private ArrayList<Kante> matchingKanten2;
	private ArrayList<Integer> startknoten;
	private boolean knotenVerfuegbar[];
	private int matchingWahl = 1;
	public PathGrowingWithCirclePossibility(Graph g) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		this.matchingGraph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten = new ArrayList<Kante>();
		this.matchingKanten2 = new ArrayList<Kante>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		this.startknoten = new ArrayList<Integer>();
		int knoten;
		while((knoten = getFreienKnoten()) != -1) {
			this.startknoten.add(knoten);
			berechneMatching(knoten);
		}
	}
	public PathGrowingWithCirclePossibility(Graph g, int startknoten) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		this.matchingGraph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten = new ArrayList<Kante>();
		this.matchingKanten2 = new ArrayList<Kante>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		int knoten;
		this.startknoten = new ArrayList<Integer>();
		this.startknoten.add(startknoten);
		berechneMatching(startknoten);
		this.matchingWahl = 1;
		while((knoten = getFreienKnoten()) != -1) {
			this.startknoten.add(knoten);
			berechneMatching(knoten);
			this.matchingWahl = 1;
		}
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe = new Graph(this.anzahlKnoten);
		Graph rueckgabe2 = new Graph(this.anzahlKnoten);
		for(int i = 0; i < this.matchingKanten.size(); i++) {
			rueckgabe.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		for(int i = 0; i < this.matchingKanten2.size(); i++) {
			rueckgabe2.insertKante(matchingKanten2.get(i).getKnoten1(), matchingKanten2.get(i).getKnoten2(), matchingKanten2.get(i).getGewicht());
		}
		if(matchingGewicht1 > matchingGewicht2)	
			return rueckgabe;
		else
			return rueckgabe2;
	}
	private void berechneMatching(int startKnoten) {
			int tmp = 0;
			int endKnoten = -1;
			int weiterKnoten = -1;
			boolean ende = false;
			for(int i = 0; i < anzahlKnoten; i++) {
				if(graph[startKnoten][i]>tmp && !knotenVerfuegbar[i]) {
					tmp = graph[startKnoten][i];
					endKnoten = i;
					weiterKnoten = i;
				}
			}
			if(this.matchingWahl == 2) {
				int loeschung = -1;
				for(int i = 0; i < startknoten.size(); i++) {
					if(graph[startKnoten][startknoten.get(i)]>tmp && matchingGraph[startKnoten][startknoten.get(i)] == 0 && matchingGraph[startknoten.get(i)][startKnoten] == 0) {
						tmp = graph[startKnoten][startknoten.get(i)];
						endKnoten = startknoten.get(i);
						ende = true;
						loeschung = i;
					}
					if(loeschung != -1)
						startknoten.remove(loeschung);
				}
			}
			knotenVerfuegbar[startKnoten] = true;
			if(endKnoten != -1) {	
				if(this.matchingWahl == 1) {
					matchingKanten.add(new Kante(startKnoten+1,endKnoten+1,tmp));
					matchingGraph[startKnoten][endKnoten] = tmp;
					matchingGraph[endKnoten][startKnoten] = tmp;
				}
				else {
					matchingKanten2.add(new Kante(startKnoten+1,endKnoten+1,tmp));
					matchingGraph[startKnoten][endKnoten] = tmp;
					matchingGraph[endKnoten][startKnoten] = tmp;
				}
				this.matchingWahl = 3 - this.matchingWahl;
				if(!ende)
					berechneMatching(endKnoten);
				else if(weiterKnoten != -1) {
					this.startknoten.add(weiterKnoten);
					berechneMatching(weiterKnoten);
				}
			}	
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		String rueckgabe2="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";;
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i)+"</tr>";
			this.matchingGewicht1+= matchingKanten.get(i).getGewicht();
		}
		rueckgabe += "</table>";
		for(int i = 0; i < matchingKanten2.size(); i++) {
			rueckgabe2 += "<tr>" + matchingKanten2.get(i)+"</tr>";
			this.matchingGewicht2+= matchingKanten2.get(i).getGewicht();
		}
		rueckgabe2 += "</table>";
		if(matchingGewicht1 > matchingGewicht2)	
			return rueckgabe;
		else
			return rueckgabe2;
	}
	public int getMatchingGewicht() {
		if(matchingGewicht1 > matchingGewicht2)	
			return matchingGewicht1;
		else
			return matchingGewicht2;
	}
	public int getMatching1() {
		return this.matchingGewicht1;
	}
	public int getMatching2() {
		return this.matchingGewicht2;
	}
	private int getFreienKnoten() {
		int rueckgabe = -1;
		for(int i = 0; i < anzahlKnoten;i++)
			if(!knotenVerfuegbar[i]) {
				rueckgabe = i;
				break;
			}
		return rueckgabe;
	}
	public String writeTableHTML() {
		Graph matching = new Graph(anzahlKnoten);
		if(matchingGewicht1 > matchingGewicht2) {
			for(int i = 0; i < matchingKanten.size();i++)
				matching.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		else{
			for(int i = 0; i < matchingKanten2.size();i++)
				matching.insertKante(matchingKanten2.get(i).getKnoten1(), matchingKanten2.get(i).getKnoten2(), matchingKanten2.get(i).getGewicht());
		}
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
}
