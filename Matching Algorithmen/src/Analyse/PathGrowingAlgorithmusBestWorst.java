/* Path Growing Algorithmus Best Worst
 * Bietet die zwei verschiedenen Konstruktoren PathGrowingAlgorithmusBestWorst(Graph g) und PathGrowingAlgorithmusBestWorst(Graph g, int startknoten)
 * berechneMatching1(): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben. BWBW-Fall
 * berechneMatching2(): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben. WBWB-Fall
 * getFreienKnoten1(): Suche beim Starten eines neuen Pfades nach einem neuen Startknoten. BWBW-Fall
 * getFreienKnoten2(): Suche beim Starten eines neuen Pfades nach einem neuen Startknoten. WBWB-Fall
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben
 * getMatching1() und getMatching2(): Jeweils der Wert der Belegung wird zurueckgegeben. BWBW-Fall
 * getMatching3() und getMatching4(): Jeweils der Wert der Belegung wird zurueckgegeben. WBWB-Fall
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 */
package Analyse;
import java.util.ArrayList;
import GraphKlassen.*;
public class PathGrowingAlgorithmusBestWorst {
	private int anzahlKnoten;
	private int matchingGewicht1 = 0;
	private int matchingGewicht2 = 0;
	private int matchingGewicht3 = 0;
	private int matchingGewicht4 = 0;
	private int[][] graph;
	private ArrayList<Kante> matchingKanten1;
	private ArrayList<Kante> matchingKanten2;
	private ArrayList<Kante> matchingKanten3;
	private ArrayList<Kante> matchingKanten4;
	private boolean knotenVerfuegbar[];
	private boolean knotenVerfuegbar2[];
	private int matchingWahl = 1;
	public PathGrowingAlgorithmusBestWorst(Graph g) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten1 = new ArrayList<Kante>();
		this.matchingKanten2 = new ArrayList<Kante>();
		this.matchingKanten3 = new ArrayList<Kante>();
		this.matchingKanten4 = new ArrayList<Kante>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		knotenVerfuegbar2 = new boolean[anzahlKnoten];
		int knoten;
		while((knoten = getFreienKnoten()) != -1) {
			berechneMatching1(knoten);
			this.matchingWahl = 1;
		}
		while((knoten = getFreienKnoten2()) != -1) {
			berechneMatching2(knoten);
			this.matchingWahl = 1;
		}
	}
	public PathGrowingAlgorithmusBestWorst(Graph g, int startknoten) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten1 = new ArrayList<Kante>();
		this.matchingKanten2 = new ArrayList<Kante>();
		this.matchingKanten3 = new ArrayList<Kante>();
		this.matchingKanten4 = new ArrayList<Kante>();
		knotenVerfuegbar = new boolean[anzahlKnoten];
		knotenVerfuegbar2 = new boolean[anzahlKnoten];
		int knoten;
		berechneMatching1(startknoten);
		while((knoten = getFreienKnoten()) != -1) {
			berechneMatching1(knoten);
			this.matchingWahl = 1;
		}
		this.matchingWahl = 1;
		berechneMatching2(startknoten);
		while((knoten = getFreienKnoten2()) != -1) {
			berechneMatching2(knoten);
			this.matchingWahl = 1;
		}
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe1 = new Graph(this.anzahlKnoten);
		Graph rueckgabe2 = new Graph(this.anzahlKnoten);
		Graph rueckgabe3 = new Graph(this.anzahlKnoten);
		Graph rueckgabe4 = new Graph(this.anzahlKnoten);
		for(int i = 0; i < this.matchingKanten1.size(); i++) {
			rueckgabe1.insertKante(matchingKanten1.get(i).getKnoten1(), matchingKanten1.get(i).getKnoten2(), matchingKanten1.get(i).getGewicht());
		}
		for(int i = 0; i < this.matchingKanten2.size(); i++) {
			rueckgabe2.insertKante(matchingKanten2.get(i).getKnoten1(), matchingKanten2.get(i).getKnoten2(), matchingKanten2.get(i).getGewicht());
		}
		for(int i = 0; i < this.matchingKanten3.size(); i++) {
			rueckgabe3.insertKante(matchingKanten3.get(i).getKnoten1(), matchingKanten3.get(i).getKnoten2(), matchingKanten3.get(i).getGewicht());
		}
		for(int i = 0; i < this.matchingKanten4.size(); i++) {
			rueckgabe4.insertKante(matchingKanten4.get(i).getKnoten1(), matchingKanten4.get(i).getKnoten2(), matchingKanten4.get(i).getGewicht());
		}
		if(matchingGewicht1 > matchingGewicht2 && matchingGewicht1 > matchingGewicht3 && matchingGewicht1 > matchingGewicht4)	
			return rueckgabe1;
		else if(matchingGewicht2 > matchingGewicht3 && matchingGewicht2 > matchingGewicht4)
			return rueckgabe2;
		else if(matchingGewicht3 > matchingGewicht4)
			return rueckgabe3;
		else
			return rueckgabe4;
	}
	private void berechneMatching1(int startKnoten) {
			int tmp;
			int endKnoten = -1;
			if(this.matchingWahl == 1) {
				tmp = 0;
				for(int i = 0; i < anzahlKnoten; i++) {
					if(graph[startKnoten][i]>tmp && !knotenVerfuegbar[i]  && i!=startKnoten) {
						tmp = graph[startKnoten][i];
						endKnoten = i;
					}
				}
			}
			else {
				tmp = Integer.MAX_VALUE;
				for(int i = 0; i < anzahlKnoten; i++) {
					if(graph[startKnoten][i]<tmp && !knotenVerfuegbar[i]  && i!=startKnoten && graph[startKnoten][i] > 0) {
						tmp = graph[startKnoten][i];
						endKnoten = i;
					}
				}
			}
			knotenVerfuegbar[startKnoten] = true;
			if(endKnoten != -1) {	
				if(this.matchingWahl == 1) 
					this.matchingKanten1.add(new Kante(startKnoten+1,endKnoten+1,tmp));
				else
					this.matchingKanten2.add(new Kante(startKnoten+1,endKnoten+1,tmp));
				this.matchingWahl = 3 - this.matchingWahl;
				berechneMatching1(endKnoten);
			}
	}
	private void berechneMatching2(int startKnoten) {
		int tmp;
		int endKnoten = -1;
		if(this.matchingWahl == 1) {
			tmp = Integer.MAX_VALUE;
			for(int i = 0; i < anzahlKnoten; i++) {
				if(graph[startKnoten][i]<tmp && !knotenVerfuegbar2[i] && i!=startKnoten && graph[startKnoten][i] > 0) {
					tmp = graph[startKnoten][i];
					endKnoten = i;
				}
			}
		}
		else {
			tmp = 0;
			for(int i = 0; i < anzahlKnoten; i++) {
				if(graph[startKnoten][i]>tmp && !knotenVerfuegbar2[i]  && i!=startKnoten) {
					tmp = graph[startKnoten][i];
					endKnoten = i;
				}
			}
		}
		knotenVerfuegbar2[startKnoten] = true;
		if(endKnoten != -1) {	
			if(this.matchingWahl == 1) 
				this.matchingKanten3.add(new Kante(startKnoten+1,endKnoten+1,tmp));
			else
				this.matchingKanten4.add(new Kante(startKnoten+1,endKnoten+1,tmp));
			this.matchingWahl = 3 - this.matchingWahl;
			berechneMatching2(endKnoten);
		}
	}
	public String writeErgebnis() {
		String rueckgabe1="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		String rueckgabe2="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		String rueckgabe3="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		String rueckgabe4="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		for(int i = 0; i < matchingKanten1.size(); i++) {
			rueckgabe1 += "<tr>" + matchingKanten1.get(i)+"</tr>";
			this.matchingGewicht1+= matchingKanten1.get(i).getGewicht();
		}
		rueckgabe1 += "</table>";
		for(int i = 0; i < matchingKanten2.size(); i++) {
			rueckgabe2 += "<tr>" + matchingKanten2.get(i)+"</tr>";
			this.matchingGewicht2+= matchingKanten2.get(i).getGewicht();
		}
		rueckgabe2 += "</table>";
		for(int i = 0; i < matchingKanten3.size(); i++) {
			rueckgabe3 += "<tr>" + matchingKanten3.get(i)+"</tr>";
			this.matchingGewicht3+= matchingKanten3.get(i).getGewicht();
		}
		rueckgabe3 += "</table>";
		for(int i = 0; i < matchingKanten4.size(); i++) {
			rueckgabe4 += "<tr>" + matchingKanten4.get(i)+"</tr>";
			this.matchingGewicht4+= matchingKanten4.get(i).getGewicht();
		}
		rueckgabe4 += "</table>";
		if(matchingGewicht1 > matchingGewicht2 && matchingGewicht1 > matchingGewicht3 && matchingGewicht1 > matchingGewicht4)	
			return rueckgabe1;
		else if(matchingGewicht2 > matchingGewicht3 && matchingGewicht2 > matchingGewicht4)
			return rueckgabe2;
		else if(matchingGewicht3 > matchingGewicht4)
			return rueckgabe3;
		else
			return rueckgabe4;
	}
	public int getMatchingGewicht() {
		if(matchingGewicht1 > matchingGewicht2 && matchingGewicht1 > matchingGewicht3 && matchingGewicht1 > matchingGewicht4)	
			return matchingGewicht1;
		else if(matchingGewicht2 > matchingGewicht3 && matchingGewicht2 > matchingGewicht4)
			return matchingGewicht2;
		else if(matchingGewicht3 > matchingGewicht4)
			return matchingGewicht3;
		else
			return matchingGewicht4;
	}
	public int getMatching1() {
		return this.matchingGewicht1;
	}
	public int getMatching2() {
		return this.matchingGewicht2;
	}
	public int getMatching3() {
		return this.matchingGewicht3;
	}
	public int getMatching4() {
		return this.matchingGewicht4;
	}
	private int getFreienKnoten() {
		int rueckgabe = -1;
		for(int i = 0; i < anzahlKnoten;i++)
			if(!knotenVerfuegbar[i]) {
				rueckgabe = i;
				break;
			}
		return rueckgabe;
	}
	private int getFreienKnoten2() {
		int rueckgabe = -1;
		for(int i = 0; i < anzahlKnoten;i++)
			if(!knotenVerfuegbar2[i]) {
				rueckgabe = i;
				break;
			}
		return rueckgabe;
	}
	public String writeTableHTML() {
		Graph matching = new Graph(anzahlKnoten);
		if(matchingGewicht1 > matchingGewicht2 && matchingGewicht1 > matchingGewicht3 && matchingGewicht1 > matchingGewicht4) {
			for(int i = 0; i < matchingKanten1.size();i++)
				matching.insertKante(matchingKanten1.get(i).getKnoten1(), matchingKanten1.get(i).getKnoten2(), matchingKanten1.get(i).getGewicht());
		}
		else if(matchingGewicht2 > matchingGewicht3 && matchingGewicht2 > matchingGewicht4){
			for(int i = 0; i < matchingKanten2.size();i++)
				matching.insertKante(matchingKanten2.get(i).getKnoten1(), matchingKanten2.get(i).getKnoten2(), matchingKanten2.get(i).getGewicht());
		}
		else if(matchingGewicht3 > matchingGewicht4){
			for(int i = 0; i < matchingKanten3.size();i++)
				matching.insertKante(matchingKanten3.get(i).getKnoten1(), matchingKanten3.get(i).getKnoten2(), matchingKanten3.get(i).getGewicht());
		}
		else{
			for(int i = 0; i < matchingKanten4.size();i++)
				matching.insertKante(matchingKanten4.get(i).getKnoten1(), matchingKanten4.get(i).getKnoten2(), matchingKanten4.get(i).getGewicht());
		}
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
}
