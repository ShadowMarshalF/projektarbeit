/* Greedy Backwards Algorithmus
 * Bietet den Konstruktor GreedyAlgorithmusBackwards(int anzahlKnoten, Graph graph)
 * berechneMatching(): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben.
 * fuelleMinHeap(): Der Min Heap wird anhand der Kantengewichte gefuellt
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 */
package Analyse;
import java.util.ArrayList;
import GraphKlassen.*;
public class GreedyAlgorithmusBackwards {
	private int anzahlKnoten;
	private ArrayList<Kante> kantenliste;
	private ArrayList<Kante> matchingKanten;
	private MinHeap Kanten;
	private int kantenGrad[];
	private int matchingGewicht = 0;
	private Graph graph;
	public GreedyAlgorithmusBackwards(int anzahlKnoten, Graph graph) {
		this.anzahlKnoten = anzahlKnoten;
		this.graph = new Graph(anzahlKnoten);
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph.insertKante(i+1, j+1, graph.getGraph()[i][j]);
			}
		}
		this.kantenliste = new ArrayList<Kante>();
		for(int i = 0; i < graph.getKantenliste().size(); i++) {
			this.kantenliste.add(graph.getKantenliste().get(i));
		}
		this.matchingKanten = new ArrayList<Kante>();
		Kanten = new MinHeap(anzahlKnoten*anzahlKnoten);
		kantenGrad = new int[anzahlKnoten];
		fuelleMinHeap();
		berechneMatching();
	}
	private void fuelleMinHeap() {
		for(int i = 0; i < kantenliste.size(); i++) {
			Kante eingabe = kantenliste.get(i);
			Kanten.insert(eingabe);
			kantenGrad[eingabe.getKnoten1()-1] += 1;
			kantenGrad[eingabe.getKnoten2()-1] += 1;
		}
	}
	private void berechneMatching() {
		Kante kleinsteKante;
		while((kleinsteKante = Kanten.remove()) != null){
			if(kantenGrad[kleinsteKante.getKnoten1()-1] == 0 || kantenGrad[kleinsteKante.getKnoten2()-1] == 0) {
				
			}
			else if(kantenGrad[kleinsteKante.getKnoten1()-1] == 1 && kantenGrad[kleinsteKante.getKnoten2()-1] == 1) {
				kantenGrad[kleinsteKante.getKnoten1()-1] = 0;
				kantenGrad[kleinsteKante.getKnoten2()-1] = 0;
				int[][] graphTest = this.graph.getGraph();
				matchingKanten.add(new Kante(kleinsteKante.getKnoten1(),kleinsteKante.getKnoten2(),graphTest[kleinsteKante.getKnoten1()-1][kleinsteKante.getKnoten2()-1]));
				matchingGewicht += graphTest[kleinsteKante.getKnoten1()-1][kleinsteKante.getKnoten2()-1];
				graph.updateKante(kleinsteKante.getKnoten1(), kleinsteKante.getKnoten2(), 0);
				graph.updateKante(kleinsteKante.getKnoten2(), kleinsteKante.getKnoten1(), 0);
			} else {
				kantenGrad[kleinsteKante.getKnoten1()-1] -= 1;
				kantenGrad[kleinsteKante.getKnoten2()-1] -= 1;
				graph.updateKante(kleinsteKante.getKnoten1(), kleinsteKante.getKnoten2(), 0);
				graph.updateKante(kleinsteKante.getKnoten2(), kleinsteKante.getKnoten1(), 0);
				if(kantenGrad[kleinsteKante.getKnoten1()-1] == 1) {
					int speicherKnoten = 0;
					int[][] graphTest = this.graph.getGraph();
					for(int i = 0; i < this.graph.getAnzahlKnoten(); i++) {
						if(graphTest[i][kleinsteKante.getKnoten1()-1] != 0) {
							speicherKnoten = i;
							matchingKanten.add(new Kante(i+1,kleinsteKante.getKnoten1(),graphTest[i][kleinsteKante.getKnoten1()-1]));
						}
					}
					kantenGrad[kleinsteKante.getKnoten1()-1] = 0;
					kantenGrad[speicherKnoten] = 0;
					matchingGewicht += graphTest[speicherKnoten][kleinsteKante.getKnoten1()-1];
					graph.updateKante(kleinsteKante.getKnoten1(), speicherKnoten+1, 0);
					graph.updateKante(speicherKnoten+1, kleinsteKante.getKnoten1(), 0);
					for(int i = 0; i < this.graph.getAnzahlKnoten(); i++) {
						if(graphTest[i][speicherKnoten] != 0) {
							kantenGrad[i] -= 1;
							graph.updateKante(speicherKnoten+1, i+1, 0);
							graph.updateKante(i+1, speicherKnoten+1, 0);
						}
					}
				}
				if(kantenGrad[kleinsteKante.getKnoten2()-1] == 1) {
					int speicherKnoten = 0;
					int[][] graphTest = this.graph.getGraph();
					for(int i = 0; i < this.graph.getAnzahlKnoten(); i++) {
						if(graphTest[i][kleinsteKante.getKnoten2()-1] != 0) {
							speicherKnoten = i;
							matchingKanten.add(new Kante(i+1,kleinsteKante.getKnoten2(),graphTest[i][kleinsteKante.getKnoten2()-1]));
						}
					}
					kantenGrad[kleinsteKante.getKnoten2()-1] = 0;
					kantenGrad[speicherKnoten] = 0;
					matchingGewicht += graphTest[speicherKnoten][kleinsteKante.getKnoten2()-1];
					graph.updateKante(kleinsteKante.getKnoten2(), speicherKnoten+1, 0);
					graph.updateKante(speicherKnoten+1, kleinsteKante.getKnoten2(), 0);
					for(int i = 0; i < this.graph.getAnzahlKnoten(); i++) {
						if(graphTest[i][speicherKnoten] != 0) {
							kantenGrad[i] -= 1;
							graph.updateKante(speicherKnoten+1, i+1, 0);
							graph.updateKante(i+1, speicherKnoten+1, 0);
						}
					}
				}
			}
		}
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe = new Graph(this.anzahlKnoten);
		for(int i = 0; i < this.matchingKanten.size(); i++) {
			rueckgabe.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		
		return rueckgabe;
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i)+"</tr>";
		}
		rueckgabe += "</table>";
		return rueckgabe;
	}
	public int getMatchingGewicht() {
		return matchingGewicht;
	}
	public String writeTableHTML() {
		Graph matching = new Graph(anzahlKnoten);
		for(int i = 0; i < matchingKanten.size();i++)
			matching.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
}
