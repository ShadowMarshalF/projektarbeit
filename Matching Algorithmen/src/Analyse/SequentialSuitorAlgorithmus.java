/* Sequential Suitor Algorithmus
 * Bietet den Konstruktor SequentialSuitorAlgorithmus(Graph g)
 * generiereMatching(): Ist die eigentliche Berechnung des Algorithmus, wie in der Masterarbeit beschrieben.
 * getMatchingGraph(): Der Matching-Graph wird zurueckgegeben
 * getMatchingGewicht(): Der Matching-Wert wird zurueckgegeben
 * writeErgebnis(): Liste der Matching-Kanten wird als HTML-Code zurueckgegeben 
 * writeTableHTML(): Tabelle des Matchings wird als HTML-Code zurueckgegeben
 */
package Analyse;

import java.util.ArrayList;

import GraphKlassen.Graph;
import GraphKlassen.Kante;

public class SequentialSuitorAlgorithmus {
	private int anzahlKnoten;
	private int[][] graph;
	private int[] suitor;
	private int[] ws;
	private int matchingGewicht;
	private ArrayList<Kante> matchingKanten;
	public SequentialSuitorAlgorithmus(Graph g) {
		this.anzahlKnoten = g.getAnzahlKnoten();
		this.graph = new int[anzahlKnoten][anzahlKnoten];
		for(int i = 0; i<anzahlKnoten; i++) {
			for(int j = 0; j < anzahlKnoten; j++) {
				this.graph[i][j] = g.getGraph()[i][j];
			}
		}
		this.matchingKanten = new ArrayList<Kante>();
		suitor = new int[this.anzahlKnoten];
		ws = new int[this.anzahlKnoten];
		generiereMatching();
	}
	private void generiereMatching() {
		for(int u = 0; u < this.anzahlKnoten; u++) {
			suitor[u] = -1;
			ws[u] = 0;
		}
		for(int u = 0; u < this.anzahlKnoten; u++) {
			int current = u;
			boolean done = false;
			while(done == false){
				int partner = suitor[current];
				int heaviest = ws[current];
				for(int v = 0; v < this.anzahlKnoten; v++) {
					if(this.graph[current][v] > heaviest && this.graph[current][v] > this.ws[v]) {
						partner = v;
						heaviest = this.graph[current][v];
					}
				}
				done = true;	
				if(heaviest > 0) {
					int y = suitor[partner];
					suitor[partner] = current;
					ws[partner] = heaviest;
					if(y != -1) {
						current = y;
						done = false;
					}
				}
			}
		}
		boolean frei[] = new boolean[this.anzahlKnoten];
		for(int i = 0; i < this.anzahlKnoten; i++) {
			if(suitor[i] != -1 && frei[i] == false) {
				frei[i] = true;
				frei[suitor[i]] = true;
				this.matchingKanten.add(new Kante(i+1, suitor[i]+1, ws[i]));
			}
				
		}
	}
	public Graph getMatchingGraph() {
		Graph rueckgabe = new Graph(this.anzahlKnoten);
		for(int i = 0; i < this.matchingKanten.size(); i++) {
			rueckgabe.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		}
		
		return rueckgabe;
	}
	public String writeErgebnis() {
		String rueckgabe="<table align=\"center\"><tr><td>1. Knoten</td><td>2. Knoten</td><td>Gewicht der Kante</td>";
		for(int i = 0; i < matchingKanten.size(); i++) {
			rueckgabe += "<tr>" + matchingKanten.get(i)+"</tr>";
			this.matchingGewicht+= matchingKanten.get(i).getGewicht();
		}
		rueckgabe += "</table>";
		return rueckgabe;
	}
	public int getMatchingGewicht() {
		return matchingGewicht;
	}
	public String writeTableHTML() {
		Graph matching = new Graph(anzahlKnoten);
		for(int i = 0; i < matchingKanten.size();i++)
			matching.insertKante(matchingKanten.get(i).getKnoten1(), matchingKanten.get(i).getKnoten2(), matchingKanten.get(i).getGewicht());
		int[][] m = matching.getGraph();
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahlKnoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahlKnoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahlKnoten; j++) {
				if(m[i][j]>0)
					ausgabe += "<td><font color = \"red\">" + m[i][j]+"</font></td>";
				else
					ausgabe += "<td>" + m[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
}
