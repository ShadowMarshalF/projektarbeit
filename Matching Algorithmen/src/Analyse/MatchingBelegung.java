/* Matching Belegung
 * Bietet den Konstruktor MatchingBelegung(int startknoten)
 * insert(m1): Eine Kante zur Belegung 1 hinzufuegen
 * insert(m2): Eine Kante zur Belegung 2 hinzufuegen
 * getBesteBelegung(): Eine ArrayList aller Matching-Kanten wird zurueckgegeben
 * getBessereBelegung(): Die bessere Belegung (1 oder 2) wird zurueckgegeben
 * getStartknoten(): Der Startknoten wird zurueckgegeben
 */
package Analyse;

import java.util.ArrayList;

import GraphKlassen.Kante;

public class MatchingBelegung {
	private int M1Wert;
	private int M2Wert;
	private ArrayList<Kante> M1Kanten;
	private ArrayList<Kante> M2Kanten;
	private int startknoten;
	public MatchingBelegung(int startknoten) {
		M1Wert = 0;
		M2Wert = 0;
		M1Kanten = new ArrayList<Kante>();
		M2Kanten = new ArrayList<Kante>();
		this.startknoten = startknoten;
	}
	public void insertM1(Kante m1) {
		M1Kanten.add(m1);
		M1Wert += m1.getGewicht();
	}
	public void insertM2(Kante m2) {
		M2Kanten.add(m2);
		M2Wert += m2.getGewicht();
	}
	public ArrayList<Kante> getBesteBelegung() {
		if(M1Wert > M2Wert)
			return M1Kanten;
		else
			return M2Kanten;
	}
	public int getBessereBelegung() {
		if(M1Wert > M2Wert)
			return 1;
		else
			return 2;
	}
	public int getStartknoten() {
		return this.startknoten;
	}
}
