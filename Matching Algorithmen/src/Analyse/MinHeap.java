package Analyse;
import GraphKlassen.*;
//Java implementation of Min Heap 
public class MinHeap { 
 private Kante[] Heap; 
 private int size; 
 private int maxsize; 

 private static final int FRONT = 1; 

 public MinHeap(int maxsize) 
 { 
     this.maxsize = maxsize; 
     this.size = 0; 
     Heap = new Kante[this.maxsize + 1]; 
     Heap[0] = new Kante(0,0,Integer.MIN_VALUE); 
 } 

 // Function to return the position of 
 // the parent for the node currently 
 // at pos 
 private int parent(int pos) 
 { 
     return pos / 2; 
 } 

 // Function to return the position of the 
 // left child for the node currently at pos 
 private int leftChild(int pos) 
 { 
     return (2 * pos); 
 } 

 // Function to return the position of 
 // the right child for the node currently 
 // at pos 
 private int rightChild(int pos) 
 { 
     return (2 * pos) + 1; 
 } 

 // Function that returns true if the passed 
 // node is a leaf node 
 private boolean isLeaf(int pos) 
 { 
     if (pos >= (size / 2) && pos <= size) { 
         return true; 
     } 
     return false; 
 } 

 // Function to swap two nodes of the heap 
 private void swap(int fpos, int spos) 
 { 
     Kante tmp; 
     tmp = Heap[fpos]; 
     Heap[fpos] = Heap[spos]; 
     Heap[spos] = tmp; 
 } 
 private void minHeapify(int pos) 
 { 
     if (isLeaf(pos)) 
         return; 

     if (Heap[pos].getGewicht() > Heap[leftChild(pos)].getGewicht() ||  
         Heap[pos].getGewicht() > Heap[rightChild(pos)].getGewicht()) { 

         if (Heap[leftChild(pos)].getGewicht() < Heap[rightChild(pos)].getGewicht()) { 
             swap(pos, leftChild(pos)); 
             minHeapify(leftChild(pos)); 
         } 
         else { 
             swap(pos, rightChild(pos)); 
             minHeapify(rightChild(pos)); 
         } 
     } 
 } 

 // Function to insert a node into the heap 
 public void insert(Kante element) 
 { 
	 this.size += 1;
     Heap[this.size] = element; 
     int current = this.size; 

     while (Heap[current].getGewicht() < Heap[parent(current)].getGewicht()) { 
         swap(current, parent(current)); 
         current = parent(current); 
     } 
 } 

 // Function to print the contents of the heap 
 public void print() 
 { 
     for (int i = 1; i <= this.size / 2; i++) { 
         System.out.print(" PARENT : " + Heap[i]
                          + " LEFT CHILD : " + Heap[2 * i] 
                          + " RIGHT CHILD :" + Heap[2 * i + 1]); 
         System.out.println(); 
     } 
 } 

 // Function to build the min heap using 
 // the minHeapify 
 public void minHeap() 
 { 
     for (int pos = (this.size / 2); pos >= 1; pos--) { 
         minHeapify(pos); 
     } 
 } 

 // Function to remove and return the minimum 
 // element from the heap 
 public Kante remove() 
 { 
 	Kante popped;
 	if(this.size == 0)
 		popped = null;
 	else if(this.size == 1) {
 		this.size=0;
 		popped = Heap[1];
 	}
 	else if(this.size == 2) {
 		popped = Heap[1];
 		swap(1,2);
 		this.size=1;
 	}
 	else {
 		popped = Heap[FRONT]; 
        Heap[FRONT] = Heap[size--]; 
        minHeapify(FRONT); 
 	}
     return popped; 
 } 
} 
