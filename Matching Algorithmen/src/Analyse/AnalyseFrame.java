/* AnalyseFrame
 * Dieser Frame bietet die Auswahl verschiedene Algorithmen zu testen.
 * Er zeigt die Resultate in einer Tabelle an.
 * Geoeffnet wird der Frame ueber den Knostruktor AnalyseFrame();
 * Methoden stehen keine zur Verfuegung.
 */
package Analyse;
import GraphKlassen.*;
import Beispiele.*;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import ApproxAlgorithmen.*;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListDataListener;
import javax.swing.table.DefaultTableModel;

public class AnalyseFrame extends JFrame implements ActionListener{
	private static final long serialVersionUID = 7907367443165438056L;
	private Dimension d = this.getToolkit().getScreenSize();
	private GreedyAlgorithmus gr;
	private PathGrowingAlgorithmus pg;
	private PathGrowingAlgorithmusBestWorst pgbw;
	private GreedyAlgorithmusBackwards gab;
	private ExpandedPathGrowingAlgorithmus epg;
	private LocallyHeaviestMatchingAlgorithmus lam;
	private PathGrowingWithCirclePossibility wpg;
	private SequentialSuitorAlgorithmus ssa;
	private DisjoinedPathGrowingWithCirclePossibility epgwcp;
	private DisjoinedPathGrowing dpg;
	private Graph g;
	
	private JPanel layout = new JPanel();
	private JPanel tabellenPanel = new JPanel();
	private JPanel checkboxPanel = new JPanel();
	private JLabel analyseLabel = new JLabel("Analyse");
	
	private JCheckBox greedy;
	private JCheckBox greedyBackwards;
	private JCheckBox pathgrowing;
	private JCheckBox pathgrowingbestworse;
	private JCheckBox expandedpathgrowing;
	private JCheckBox locallyheaviestmatching;
	private JCheckBox sequentialsuitoralgorithm;
	private JCheckBox pathgrowingwithcirclepossibility;
	private JCheckBox disjoinedpathgrowingwithcirclepossibility;
	private JCheckBox disjoinedpathgrowing;
	private JComboBox<String> algorithmusauswahl;
	private JComboBox<String> algorithmusvarianten;
	private JComboBox<String> graphauswahl;
	private JButton analyseButton;
	private DefaultComboBoxModel<String> leerModel;
	private DefaultComboBoxModel<String> greedyModel;
	private DefaultComboBoxModel<String> pgModel;
	private DefaultComboBoxModel<String> vpgModel;
	private DefaultComboBoxModel<String> lhmModel;
	private DefaultComboBoxModel<String> dspgwcpModel;
	private Beispiele beispiele;
	private String leerliste[] = {""};
	private String beispielliste[] = {"Beispiel 1 - 4 Knoten", "Beispiel 2 - 6 Knoten", "Beispiel 3 - 8 Knoten", "Beispiel 4 - var. Knoten","Beispiel 5 - 48 Knoten", "Graph aus Datei laden", "Aus Datei geladenen Graph weiternutzen"};
	private String comboboxliste[] = {"Greedy Algorithmus", "Path-Growing Algorithmus", "Path Growing Best Worse" , "Expanded Path-Growing Algorithmus", "LAM Algorithmus", "Vergleichen von Algorithmen", "Disjoined Path Growing with Circle Possibility", "Disjoined Path Growing"};
	
	private String comboboxlistegreedy[] = {"GreedyBackwards"};
	private String comboboxlistePG[] = {"Alle Startknoten berechnen", "Alle Werte auslesen", "Alle Berechnungsvarianten durchrechnen"};
	private String comboboxlisteVPG[] = {"Auswahl der Checkboxen", "Vergleich aller PG-Algorithmen (alle Matchings)", "Auswahl Checkboxen, für große Graphen, nur 1 Startknoten"};
	private String comboboxlisteLHM[] = {"Alle Startkanten berechnen"};
	private String comboboxlisteDSPGWCP[] = {"Korrektes Matching"};
	private JTable ausgabeTabelle;
	private JScrollPane pane;
	private File file;
	public AnalyseFrame() {
		super("Analyse");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(new Dimension(1200, 800));
		setLocation((int) ((d.getWidth() - this.getWidth()) / 2), (int) ((d.getHeight() - this.getHeight()) / 1.75));
		setLayout(new BorderLayout());
		layout.setLayout(new GridLayout(5,1));
		
		leerModel = new DefaultComboBoxModel(leerliste);
	    greedyModel = new DefaultComboBoxModel(comboboxlistegreedy);
	    pgModel = new DefaultComboBoxModel(comboboxlistePG);
	    vpgModel = new DefaultComboBoxModel(comboboxlisteVPG);
	    lhmModel = new DefaultComboBoxModel(comboboxlisteLHM);
	    dspgwcpModel = new DefaultComboBoxModel(comboboxlisteDSPGWCP);
	    ausgabeTabelle = new JTable(0, 0);
	    ausgabeTabelle.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    analyseButton = new JButton("Analyse starten");
	    beispiele = new Beispiele();
	    greedy = new JCheckBox("Greedy Algorithmus");
	    greedyBackwards = new JCheckBox("Greedy Backwards");
	    pathgrowing = new JCheckBox("Path Growing");
	    pathgrowingbestworse = new JCheckBox("PG BestWorst");
	    expandedpathgrowing = new JCheckBox("Expanded PG");
	    locallyheaviestmatching = new JCheckBox("Locally Heaviest M.");
	    sequentialsuitoralgorithm = new JCheckBox("Seq. Sui. Alg.");
	    pathgrowingwithcirclepossibility = new JCheckBox("PG With Circle P.");
	    disjoinedpathgrowingwithcirclepossibility = new JCheckBox("Disjoined PGWCP");
	    disjoinedpathgrowing = new JCheckBox("Disjoined PG");
	    checkboxPanel.setLayout(new GridLayout(1,10));
	    checkboxPanel.add(greedy);
	    checkboxPanel.add(greedyBackwards);
	    checkboxPanel.add(pathgrowing);
	    checkboxPanel.add(disjoinedpathgrowing);
	    checkboxPanel.add(pathgrowingbestworse);
	    checkboxPanel.add(expandedpathgrowing);
	    checkboxPanel.add(locallyheaviestmatching);
	    checkboxPanel.add(sequentialsuitoralgorithm);
	    checkboxPanel.add(pathgrowingwithcirclepossibility);
	    checkboxPanel.add(disjoinedpathgrowingwithcirclepossibility);
	    checkboxPanel.setVisible(false);
		algorithmusauswahl = new JComboBox(comboboxliste);
		graphauswahl = new JComboBox(beispielliste);
		algorithmusvarianten = new JComboBox();
		algorithmusvarianten.setModel(greedyModel);
		layout.add(algorithmusauswahl);
		layout.add(algorithmusvarianten);
		layout.add(graphauswahl);
		layout.add(checkboxPanel);
		layout.add(analyseButton);
		pane = new JScrollPane(ausgabeTabelle);
		tabellenPanel.add(pane);
		ausgabeTabelle.setAutoCreateRowSorter(true);
		add(layout, BorderLayout.NORTH);
		add(pane, BorderLayout.CENTER);
		add(analyseLabel, BorderLayout.SOUTH);
		algorithmusauswahl.addActionListener(this);
		analyseButton.addActionListener(this);
		greedy.addActionListener(this);
		greedyBackwards.addActionListener(this);
		pathgrowing.addActionListener(this);
		pathgrowingbestworse.addActionListener(this);
		expandedpathgrowing.addActionListener(this);
		locallyheaviestmatching.addActionListener(this);
		file = null;
		g = null;
	}
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == this.algorithmusauswahl) {
			switch(this.algorithmusauswahl.getSelectedIndex()) {
				case(1): algorithmusvarianten.setModel(pgModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false); break;
				case(2): algorithmusvarianten.setModel(pgModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false); break;
				case(3): algorithmusvarianten.setModel(pgModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false); break;
				case(4): algorithmusvarianten.setModel(lhmModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false); break;
				case(5): algorithmusvarianten.setModel(vpgModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(true); break;
				case(6): algorithmusvarianten.setModel(dspgwcpModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false); break;
				case(7): algorithmusvarianten.setModel(dspgwcpModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false); break;
				default: algorithmusvarianten.setModel(greedyModel); algorithmusvarianten.setSelectedIndex(0); checkboxPanel.setVisible(false);
			}
		}
		
		else if(ae.getSource() == this.analyseButton) {
			int algo = this.algorithmusauswahl.getSelectedIndex();
			int variante = this.algorithmusvarianten.getSelectedIndex();
			int graphindex = this.graphauswahl.getSelectedIndex();
			GraphLaden gl;
			switch(graphindex) {
				case(1): this.g = beispiele.getGraphbsp2wei(); break;
				case(2): this.g = beispiele.getGraphbsp3wei(); break;
				case(3): this.g = beispiele.getGraphbsp4wei(); break;
				case(4): this.g = beispiele.getGraphbsp5wei(); break;
				case(5):
				try {
					JFileChooser chooser;
					chooser = new JFileChooser();
					chooser.showOpenDialog(null);
					file = chooser.getSelectedFile();
					setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
					gl = new GraphLaden(file);
					this.g = gl.getGraph();
					this.graphauswahl.setSelectedIndex(6);
					setCursor(Cursor.getDefaultCursor());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					setCursor(Cursor.getDefaultCursor());
					algo = -1;
					
				} 
				 break;
				case(6): if(this.file == null) { JOptionPane.showMessageDialog(null, "Sie müssen zunächst einen Graphen aus einer Datei laden!"); algo = -1; }break;	
				default: this.g = beispiele.getGraphbsp1wei();
			}
			if(algo == 0 && variante == 0) {
				this.gab = new GreedyAlgorithmusBackwards(this.g.getAnzahlKnoten(), this.g);
				Vector colNames = new Vector();
				colNames.add("Variante");
				colNames.add("Ergebnis");
				Vector rows = new Vector();
				Vector row = new Vector();
				row.add("Greedy Backwards");
				row.add(""+this.gab.getMatchingGewicht());
				rows.add(row);
				final String[] dataType = { "STRING", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			
			else if(algo == 1 && variante == 0) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Ergebnis");
				
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					this.pg = new PathGrowingAlgorithmus(this.g, i);
					this.pg.writeErgebnis();
					int ergebnis = this.pg.getMatchingGewicht();
					row.add(""+ergebnis);
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 1 && variante == 1) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("AnzahlKanten");
				colNames.add("Summe aller Kanten");
				colNames.add("1. groesste Kante");
				colNames.add("2. groesste Kante");
				colNames.add("3. groesste Kante");
				colNames.add("kleinste Kante");
				colNames.add("Ergebnis");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					row.add(bewertung[i][0]);
					row.add(bewertung[i][1]);
					row.add(bewertung[i][2]);
					row.add(bewertung[i][3]);
					row.add(bewertung[i][4]);
					row.add(bewertung[i][5]);
					this.pg = new PathGrowingAlgorithmus(this.g, i);
					this.pg.writeErgebnis();
					int ergebnis = this.pg.getMatchingGewicht();
					row.add(""+ergebnis);
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC","NUMERIC", "NUMERIC","NUMERIC", "NUMERIC","NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 1 && variante == 2) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Summe Kanten / Anzahl");
				colNames.add("1. - 2.");
				colNames.add("1. - kleinste Kante");
				colNames.add("Differenz 1.-2. & 1.-kleinste");
				colNames.add("1. - (2.+3./2)");
				colNames.add("Differenz der linken beiden");
				colNames.add("Differenz 1.-2. & Dursch. alle r. Kanten");
				colNames.add("Differenz 1.-2. & 1.-3.");
				colNames.add("1.-2. + 1.-3.");
				colNames.add("1. - (Summe r. Kanten / Anz. r. Kanten)");
				colNames.add("1. - Summe Kanten / Anzahl");
				colNames.add("Ergebnis");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					double wert1, wert2;
					Vector row = getBewertung(bewertung, i);
					this.pg = new PathGrowingAlgorithmus(this.g, i);
					this.pg.writeErgebnis();
					int ergebnis = this.pg.getMatchingGewicht();
					// Ergebnis
					row.add(Double.parseDouble(""+ergebnis));
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			
			else if(algo == 2 && variante == 0) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Ergebnis");
				
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					this.pgbw = new PathGrowingAlgorithmusBestWorst(this.g, i);
					this.pgbw.writeErgebnis();
					int ergebnis = this.pgbw.getMatchingGewicht();
					row.add(""+ergebnis);
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 2 && variante == 1) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("AnzahlKanten");
				colNames.add("Summe aller Kanten");
				colNames.add("1. groesste Kante");
				colNames.add("2. groesste Kante");
				colNames.add("3. groesste Kante");
				colNames.add("kleinste Kante");
				colNames.add("Ergebnis");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					row.add(bewertung[i][0]);
					row.add(bewertung[i][1]);
					row.add(bewertung[i][2]);
					row.add(bewertung[i][3]);
					row.add(bewertung[i][4]);
					row.add(bewertung[i][5]);
					this.pgbw = new PathGrowingAlgorithmusBestWorst(this.g, i);
					this.pgbw.writeErgebnis();
					int ergebnis = this.pgbw.getMatchingGewicht();
					row.add(""+ergebnis);
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC","NUMERIC", "NUMERIC","NUMERIC", "NUMERIC","NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 2 && variante == 2) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Summe Kanten / Anzahl");
				colNames.add("1. - 2.");
				colNames.add("1. - kleinste Kante");
				colNames.add("Differenz 1.-2. & 1.-kleinste");
				colNames.add("1. - (2.+3./2)");
				colNames.add("Differenz der linken beiden");
				colNames.add("Differenz 1.-2. & Dursch. alle r. Kanten");
				colNames.add("Differenz 1.-2. & 1.-3.");
				colNames.add("1.-2. + 1.-3.");
				colNames.add("1. - (Summe r. Kanten / Anz. r. Kanten)");
				colNames.add("1. - Summe Kanten / Anzahl");
				colNames.add("Ergebnis");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = getBewertung(bewertung, i);
					this.pgbw = new PathGrowingAlgorithmusBestWorst(this.g, i);
					this.pgbw.writeErgebnis();
					int ergebnis = this.pgbw.getMatchingGewicht();
					// Ergebnis
					row.add(Double.parseDouble(""+ergebnis));
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 3 && variante == 0) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Ergebnis");
				
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					this.epg = new ExpandedPathGrowingAlgorithmus(this.g, i);
					this.epg.writeErgebnis();
					int ergebnis = this.epg.getMatchingGewicht();
					row.add(""+ergebnis);
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}

			else if(algo == 3 && variante == 1) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("AnzahlKanten");
				colNames.add("Summe aller Kanten");
				colNames.add("1. groesste Kante");
				colNames.add("2. groesste Kante");
				colNames.add("3. groesste Kante");
				colNames.add("kleinste Kante");
				colNames.add("Ergebnis");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					row.add(bewertung[i][0]);
					row.add(bewertung[i][1]);
					row.add(bewertung[i][2]);
					row.add(bewertung[i][3]);
					row.add(bewertung[i][4]);
					row.add(bewertung[i][5]);
					this.epg = new ExpandedPathGrowingAlgorithmus(this.g, i);
					this.epg.writeErgebnis();
					int ergebnis = this.epg.getMatchingGewicht();
					row.add(""+ergebnis);
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC","NUMERIC", "NUMERIC","NUMERIC", "NUMERIC","NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}

			else if(algo == 3 && variante == 2) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Summe Kanten / Anzahl");
				colNames.add("1. - 2.");
				colNames.add("1. - kleinste Kante");
				colNames.add("Differenz 1.-2. & 1.-kleinste");
				colNames.add("1. - (2.+3./2)");
				colNames.add("Differenz der linken beiden");
				colNames.add("Differenz 1.-2. & Dursch. alle r. Kanten");
				colNames.add("Differenz 1.-2. & 1.-3.");
				colNames.add("(1.-2. + 1.-3.)/2");
				colNames.add("1. - (Summe r. Kanten / Anz. r. Kanten)");
				colNames.add("1. - Summe Kanten / Anzahl");
				colNames.add("Ergebnis");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = getBewertung(bewertung, i);
					this.epg = new ExpandedPathGrowingAlgorithmus(this.g, i);
					this.epg.writeErgebnis();
					int ergebnis = this.epg.getMatchingGewicht();
					// Ergebnis
					row.add(Double.parseDouble(""+ergebnis));
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 4 && variante == 0) {
				Vector colNames = new Vector();
				colNames.add("Kante");
				colNames.add("Ergebnis");
				int zaehler = 0;
				double lamDurchschnitt = 0;
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					for(int j = i+1; j < this.g.getAnzahlKnoten(); j++) {
						if(this.g.getGraph()[i][j] > 0) {
							zaehler++;
							Vector row = new Vector();
							row.add((i+1) + "->" + (j+1));
							this.lam = new LocallyHeaviestMatchingAlgorithmus(this.g, i, j);
							this.lam.writeErgebnis();
							int ergebnis = this.lam.getMatchingGewicht();
							lamDurchschnitt += ergebnis;
							row.add(Double.parseDouble(""+ergebnis));
							rows.add(row);
						}
					}
				}
				Vector row = new Vector();
				row.add("Durchschnitt");
				row.add(lamDurchschnitt/zaehler);
				rows.add(row);
				final String[] dataType = { "STRING", "Double"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 5 && variante == 0) {
				int zaehler = 1;
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				double GDS=0;
				double GBWDS=0;
				double PGDS=0;
				double BWPGDS=0;
				double EPGDS=0;
				double LAMDS=0;
				double SSADS=0;
				double WPGDS=0;
				double EPGWCP=0;
				double DPGDS=0;
				double Gzeit=0;
				double GBWzeit=0;
				double PGzeit=0;
				double PGBWzeit=0;
				double EPGzeit=0;
				double LAMzeit=0;
				double SSAzeit=0;
				double PGWCPzeit=0;
				double DSPGWCPzeit=0;
				double DPGzeit=0;
				int greedywert=0;
				int lamwert=0;
				int ssawert=0;
				int greedyBWwert=0;
				if(greedy.isSelected()) {
					zaehler++;
					colNames.add("Greedy");
					long start = System.currentTimeMillis();
					gr = new GreedyAlgorithmus(this.g.getAnzahlKnoten(), this.g.getKantenliste());
					long zeit = System.currentTimeMillis() - start;
					Gzeit += zeit;
					gr.writeErgebnis();
					greedywert = gr.getMatchingGewicht();
				}
				if(greedyBackwards.isSelected()) {
					zaehler++;
					colNames.add("GreedyBW");
					long start = System.currentTimeMillis();
					gab = new GreedyAlgorithmusBackwards(this.g.getAnzahlKnoten(), this.g);
					long zeit = System.currentTimeMillis() - start;
					GBWzeit += zeit;
					
					gab.writeErgebnis();
					greedyBWwert = gab.getMatchingGewicht();
				}
				if(pathgrowing.isSelected()) {
					zaehler++;
					colNames.add("PathGrowing");
					
				}
				if(disjoinedpathgrowing.isSelected()) {
					zaehler++;
					colNames.add("DisjoinedPG");
					
				}
				if(pathgrowingbestworse.isSelected()) {
					zaehler++;
					colNames.add("PathGrowingBW");
					
				}
				if(expandedpathgrowing.isSelected()) {
					zaehler++;
					colNames.add("ExpandedPathGrowing");
					
				}
				if(locallyheaviestmatching.isSelected()) {
					zaehler++;
					colNames.add("Locally Heaviest Matching");
					long start = System.currentTimeMillis();
					lam = new LocallyHeaviestMatchingAlgorithmus(this.g);
					long zeit = System.currentTimeMillis() - start;
					LAMzeit += zeit;
					lam.writeErgebnis();
					lamwert = lam.getMatchingGewicht();
				}
				if(sequentialsuitoralgorithm.isSelected()) {
					zaehler++;
					colNames.add("Sequential Suitor Algorithmus");
					long start = System.currentTimeMillis();
					ssa = new SequentialSuitorAlgorithmus(this.g);
					gr = new GreedyAlgorithmus(this.g.getAnzahlKnoten(), this.g.getKantenliste());
					long zeit = System.currentTimeMillis() - start;
					SSAzeit += zeit;
					ssa.writeErgebnis();
					ssawert = ssa.getMatchingGewicht();
				}
				if(pathgrowingwithcirclepossibility.isSelected()) {
					zaehler++;
					colNames.add("Path Growing With Circle Possibility");
					
				}
				if(disjoinedpathgrowingwithcirclepossibility.isSelected()) {
					zaehler++;
					colNames.add("Disjoined PGWCP");
				}
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					if(greedy.isSelected()) {
						row.add(Double.parseDouble(""+greedywert));
						GDS += greedywert;
					}
					if(greedyBackwards.isSelected()) {
						row.add(Double.parseDouble(""+greedyBWwert));
						GBWDS += greedyBWwert;
					}
					if(pathgrowing.isSelected()) {
						long start = System.currentTimeMillis();
						pg = new PathGrowingAlgorithmus(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						PGzeit += zeit;
						pg.writeErgebnis();
						row.add(Double.parseDouble(""+pg.getMatchingGewicht()));
						PGDS += pg.getMatchingGewicht();
					}
					if(disjoinedpathgrowing.isSelected()) {
						long start = System.currentTimeMillis();
						dpg = new DisjoinedPathGrowing(g, i);
						long zeit = System.currentTimeMillis() - start;
						DPGzeit += zeit;
						dpg.writeErgebnis();
						row.add(Double.parseDouble(""+dpg.getMatchingGewicht()));
						DPGDS += dpg.getMatchingGewicht();
					}
					if(pathgrowingbestworse.isSelected()) {
						long start = System.currentTimeMillis();
						pgbw = new PathGrowingAlgorithmusBestWorst(this.g, i);
						gr = new GreedyAlgorithmus(this.g.getAnzahlKnoten(), this.g.getKantenliste());
						long zeit = System.currentTimeMillis() - start;
						PGBWzeit += zeit;
						pgbw.writeErgebnis();
						row.add(Double.parseDouble(""+pgbw.getMatchingGewicht()));
						BWPGDS += pgbw.getMatchingGewicht();
					}
					if(expandedpathgrowing.isSelected()) {
						long start = System.currentTimeMillis();
						epg = new ExpandedPathGrowingAlgorithmus(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						EPGzeit += zeit;
						epg.writeErgebnis();
						row.add(Double.parseDouble(""+epg.getMatchingGewicht()));
						EPGDS += epg.getMatchingGewicht();
					}
					if(locallyheaviestmatching.isSelected()) {
						row.add(Double.parseDouble(""+lamwert));
						LAMDS += lamwert;
					}
					if(sequentialsuitoralgorithm.isSelected()) {
						row.add(Double.parseDouble(""+ssawert));
						SSADS += ssawert;
					}
					if(pathgrowingwithcirclepossibility.isSelected()) {
						long start = System.currentTimeMillis();
						wpg = new PathGrowingWithCirclePossibility(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						PGWCPzeit += zeit;
						wpg.writeErgebnis();
						row.add(Double.parseDouble(""+wpg.getMatchingGewicht()));
						WPGDS += wpg.getMatchingGewicht();
					}
					if(disjoinedpathgrowingwithcirclepossibility.isSelected()) {
						long start = System.currentTimeMillis();
						epgwcp = new DisjoinedPathGrowingWithCirclePossibility(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						DSPGWCPzeit += zeit;
						row.add(Double.parseDouble(""+epgwcp.getMatchingGewicht()));
						EPGWCP += epgwcp.getMatchingGewicht();
					}
					rows.add(row);
					double prozent = ((i+1)*100/this.g.getAnzahlKnoten());
					System.out.println(prozent+"%");
				}
				Vector row = new Vector();
				row.add(-1);
				if(greedy.isSelected())
					row.add(Double.parseDouble(""+GDS/this.g.getAnzahlKnoten()));
				if(greedyBackwards.isSelected())
					row.add(Double.parseDouble(""+GBWDS/this.g.getAnzahlKnoten()));
				if(pathgrowing.isSelected())
					row.add(Double.parseDouble(""+PGDS/this.g.getAnzahlKnoten()));
				if(disjoinedpathgrowing.isSelected())
					row.add(Double.parseDouble(""+DPGDS/this.g.getAnzahlKnoten()));
				if(pathgrowingbestworse.isSelected())
					row.add(Double.parseDouble(""+BWPGDS/this.g.getAnzahlKnoten()));
				if(expandedpathgrowing.isSelected())
					row.add(Double.parseDouble(""+EPGDS/this.g.getAnzahlKnoten()));
				if(locallyheaviestmatching.isSelected())
					row.add(Double.parseDouble(""+LAMDS/this.g.getAnzahlKnoten()));
				if(sequentialsuitoralgorithm.isSelected())
					row.add(Double.parseDouble(""+SSADS/this.g.getAnzahlKnoten()));
				if(pathgrowingwithcirclepossibility.isSelected())
					row.add(Double.parseDouble(""+WPGDS/this.g.getAnzahlKnoten()));
				if(disjoinedpathgrowingwithcirclepossibility.isSelected())
					row.add(Double.parseDouble(""+EPGWCP/this.g.getAnzahlKnoten()));
				rows.add(row);
				row = new Vector();
				row.add(-2);
				if(greedy.isSelected())
					row.add(Double.parseDouble(""+Gzeit));
				if(greedyBackwards.isSelected())
					row.add(Double.parseDouble(""+GBWzeit));
				if(pathgrowing.isSelected())
					row.add(Double.parseDouble(""+PGzeit/this.g.getAnzahlKnoten()));
				if(disjoinedpathgrowing.isSelected())
					row.add(Double.parseDouble(""+DPGzeit/this.g.getAnzahlKnoten()));
				if(pathgrowingbestworse.isSelected())
					row.add(Double.parseDouble(""+PGBWzeit/this.g.getAnzahlKnoten()));
				if(expandedpathgrowing.isSelected())
					row.add(Double.parseDouble(""+EPGzeit/this.g.getAnzahlKnoten()));
				if(locallyheaviestmatching.isSelected())
					row.add(Double.parseDouble(""+LAMzeit));
				if(sequentialsuitoralgorithm.isSelected())
					row.add(Double.parseDouble(""+SSAzeit));
				if(pathgrowingwithcirclepossibility.isSelected())
					row.add(Double.parseDouble(""+PGWCPzeit/this.g.getAnzahlKnoten()));
				if(disjoinedpathgrowingwithcirclepossibility.isSelected())
					row.add(Double.parseDouble(""+DSPGWCPzeit/this.g.getAnzahlKnoten()));
				rows.add(row);
				String[] type = new String[zaehler];
				for(int i = 0; i < zaehler; i++) {
					type[i] = "DOUBLE";
				}
				final String[] dataType = type;
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 5 && variante == 1) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("Path Growing");
				colNames.add("Matching 1");
				colNames.add("Matching 2");
				colNames.add("PGBestWorse");
				colNames.add("Matching 1");
				colNames.add("Matching 2");
				colNames.add("Matching 3");
				colNames.add("Matching 4");
				colNames.add("ExpandedPG");
				colNames.add("Matching 1");
				colNames.add("Matching 2");
				colNames.add("PGWCP");
				colNames.add("Matching 1");
				colNames.add("Matching 2");
				colNames.add("DSPGWCP");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					this.pg = new PathGrowingAlgorithmus(this.g, i);
					this.pg.writeErgebnis();
					row.add(this.pg.getMatchingGewicht());
					row.add(this.pg.getMatching1());
					row.add(this.pg.getMatching2());
					this.pgbw = new PathGrowingAlgorithmusBestWorst(this.g, i);
					this.pgbw.writeErgebnis();
					row.add(this.pgbw.getMatchingGewicht());
					row.add(this.pgbw.getMatching1());
					row.add(this.pgbw.getMatching2());
					row.add(this.pgbw.getMatching3());
					row.add(this.pgbw.getMatching4());
					this.epg = new ExpandedPathGrowingAlgorithmus(this.g, i);
					this.epg.writeErgebnis();
					row.add(this.epg.getMatchingGewicht());
					row.add(this.epg.getMatching1());
					row.add(this.epg.getMatching2());
					this.wpg = new PathGrowingWithCirclePossibility(this.g, i);
					this.wpg.writeErgebnis();
					row.add(this.wpg.getMatchingGewicht());
					row.add(this.wpg.getMatching1());
					row.add(this.wpg.getMatching2());
					epgwcp = new DisjoinedPathGrowingWithCirclePossibility(this.g, i);
					row.add(this.epgwcp.getMatchingGewicht());
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC", "NUMERIC"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 6 && variante == 0) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("DSPGWCP");
				colNames.add("Korrekt");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					epgwcp = new DisjoinedPathGrowingWithCirclePossibility(this.g, i);
					//wpg.writeErgebnis();
					row.add(Double.parseDouble(""+epgwcp.getMatchingGewicht()));
					row.add(""+epgwcp.korrektesMatching());
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC", "STRING"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 5 && variante == 2) {
				int zaehler = 1;
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				double GDS=0;
				double GBWDS=0;
				double PGDS=0;
				double BWPGDS=0;
				double EPGDS=0;
				double LAMDS=0;
				double SSADS=0;
				double WPGDS=0;
				double EPGWCP=0;
				double DPGDS=0;
				double Gzeit=0;
				double GBWzeit=0;
				double PGzeit=0;
				double PGBWzeit=0;
				double EPGzeit=0;
				double LAMzeit=0;
				double SSAzeit=0;
				double PGWCPzeit=0;
				double DSPGWCPzeit=0;
				double DPGzeit=0;
				int greedywert=0;
				int lamwert=0;
				int ssawert=0;
				int greedyBWwert=0;
				if(greedy.isSelected()) {
					zaehler++;
					colNames.add("Greedy");
					long start = System.currentTimeMillis();
					gr = new GreedyAlgorithmus(this.g.getAnzahlKnoten(), this.g.getKantenliste());
					long zeit = System.currentTimeMillis() - start;
					Gzeit += zeit;
					gr.writeErgebnis();
					greedywert = gr.getMatchingGewicht();
				}
				if(greedyBackwards.isSelected()) {
					zaehler++;
					colNames.add("GreedyBW");
					long start = System.currentTimeMillis();
					gab = new GreedyAlgorithmusBackwards(this.g.getAnzahlKnoten(), this.g);
					long zeit = System.currentTimeMillis() - start;
					GBWzeit += zeit;
					
					gab.writeErgebnis();
					greedyBWwert = gab.getMatchingGewicht();
				}
				if(pathgrowing.isSelected()) {
					zaehler++;
					colNames.add("PathGrowing");
					
				}
				if(disjoinedpathgrowing.isSelected()) {
					zaehler++;
					colNames.add("DisjoinedPG");
					
				}
				if(pathgrowingbestworse.isSelected()) {
					zaehler++;
					colNames.add("PathGrowingBW");
					
				}
				if(expandedpathgrowing.isSelected()) {
					zaehler++;
					colNames.add("ExpandedPathGrowing");
					
				}
				if(locallyheaviestmatching.isSelected()) {
					zaehler++;
					colNames.add("Locally Heaviest Matching");
					long start = System.currentTimeMillis();
					lam = new LocallyHeaviestMatchingAlgorithmus(this.g);
					long zeit = System.currentTimeMillis() - start;
					LAMzeit += zeit;
					lam.writeErgebnis();
					lamwert = lam.getMatchingGewicht();
				}
				if(sequentialsuitoralgorithm.isSelected()) {
					zaehler++;
					colNames.add("Sequential Suitor Algorithmus");
					long start = System.currentTimeMillis();
					ssa = new SequentialSuitorAlgorithmus(this.g);
					gr = new GreedyAlgorithmus(this.g.getAnzahlKnoten(), this.g.getKantenliste());
					long zeit = System.currentTimeMillis() - start;
					SSAzeit += zeit;
					ssa.writeErgebnis();
					ssawert = ssa.getMatchingGewicht();
				}
				if(pathgrowingwithcirclepossibility.isSelected()) {
					zaehler++;
					colNames.add("Path Growing With Circle Possibility");
					
				}
				if(disjoinedpathgrowingwithcirclepossibility.isSelected()) {
					zaehler++;
					colNames.add("Disjoined PGWCP");
				}
				Vector rows = new Vector();
				for(int i = 0; i < 1; i++) {
					Vector row = new Vector();
					row.add(i+1);
					if(greedy.isSelected()) {
						row.add(Double.parseDouble(""+greedywert));
						GDS += greedywert;
					}
					if(greedyBackwards.isSelected()) {
						row.add(Double.parseDouble(""+greedyBWwert));
						GBWDS += greedyBWwert;
					}
					if(pathgrowing.isSelected()) {
						long start = System.currentTimeMillis();
						pg = new PathGrowingAlgorithmus(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						PGzeit += zeit;
						pg.writeErgebnis();
						row.add(Double.parseDouble(""+pg.getMatchingGewicht()));
						PGDS += pg.getMatchingGewicht();
					}
					if(disjoinedpathgrowing.isSelected()) {
						long start = System.currentTimeMillis();
						dpg = new DisjoinedPathGrowing(g, i);
						long zeit = System.currentTimeMillis() - start;
						DPGzeit += zeit;
						dpg.writeErgebnis();
						row.add(Double.parseDouble(""+dpg.getMatchingGewicht()));
						DPGDS += dpg.getMatchingGewicht();
					}
					if(pathgrowingbestworse.isSelected()) {
						long start = System.currentTimeMillis();
						pgbw = new PathGrowingAlgorithmusBestWorst(this.g, i);
						gr = new GreedyAlgorithmus(this.g.getAnzahlKnoten(), this.g.getKantenliste());
						long zeit = System.currentTimeMillis() - start;
						PGBWzeit += zeit;
						pgbw.writeErgebnis();
						row.add(Double.parseDouble(""+pgbw.getMatchingGewicht()));
						BWPGDS += pgbw.getMatchingGewicht();
					}
					if(expandedpathgrowing.isSelected()) {
						long start = System.currentTimeMillis();
						epg = new ExpandedPathGrowingAlgorithmus(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						EPGzeit += zeit;
						epg.writeErgebnis();
						row.add(Double.parseDouble(""+epg.getMatchingGewicht()));
						EPGDS += epg.getMatchingGewicht();
					}
					if(locallyheaviestmatching.isSelected()) {
						row.add(Double.parseDouble(""+lamwert));
						LAMDS += lamwert;
					}
					if(sequentialsuitoralgorithm.isSelected()) {
						row.add(Double.parseDouble(""+ssawert));
						SSADS += ssawert;
					}
					if(pathgrowingwithcirclepossibility.isSelected()) {
						long start = System.currentTimeMillis();
						wpg = new PathGrowingWithCirclePossibility(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						PGWCPzeit += zeit;
						wpg.writeErgebnis();
						row.add(Double.parseDouble(""+wpg.getMatchingGewicht()));
						WPGDS += wpg.getMatchingGewicht();
					}
					if(disjoinedpathgrowingwithcirclepossibility.isSelected()) {
						long start = System.currentTimeMillis();
						epgwcp = new DisjoinedPathGrowingWithCirclePossibility(this.g, i);
						long zeit = System.currentTimeMillis() - start;
						DSPGWCPzeit += zeit;
						row.add(Double.parseDouble(""+epgwcp.getMatchingGewicht()));
						EPGWCP += epgwcp.getMatchingGewicht();
					}
					rows.add(row);
				}
				Vector row = new Vector();
				row.add(-2);
				if(greedy.isSelected())
					row.add(Double.parseDouble(""+Gzeit));
				if(greedyBackwards.isSelected())
					row.add(Double.parseDouble(""+GBWzeit));
				if(pathgrowing.isSelected())
					row.add(Double.parseDouble(""+PGzeit));
				if(disjoinedpathgrowing.isSelected())
					row.add(Double.parseDouble(""+DPGzeit));
				if(pathgrowingbestworse.isSelected())
					row.add(Double.parseDouble(""+PGBWzeit));
				if(expandedpathgrowing.isSelected())
					row.add(Double.parseDouble(""+EPGzeit));
				if(locallyheaviestmatching.isSelected())
					row.add(Double.parseDouble(""+LAMzeit));
				if(sequentialsuitoralgorithm.isSelected())
					row.add(Double.parseDouble(""+SSAzeit));
				if(pathgrowingwithcirclepossibility.isSelected())
					row.add(Double.parseDouble(""+PGWCPzeit));
				if(disjoinedpathgrowingwithcirclepossibility.isSelected())
					row.add(Double.parseDouble(""+DSPGWCPzeit));
				rows.add(row);
				String[] type = new String[zaehler];
				for(int i = 0; i < zaehler; i++) {
					type[i] = "DOUBLE";
				}
				final String[] dataType = type;
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
			else if(algo == 7 && variante == 0) {
				Vector colNames = new Vector();
				colNames.add("Startknoten");
				colNames.add("DPG");
				colNames.add("Korrekt");
				double[][] bewertung = this.g.getKnotenDurchschnitte();
				Vector rows = new Vector();
				for(int i = 0; i < this.g.getAnzahlKnoten(); i++) {
					Vector row = new Vector();
					row.add(i+1);
					dpg = new DisjoinedPathGrowing(this.g, i);
					//wpg.writeErgebnis();
					row.add(Double.parseDouble(""+dpg.getMatchingGewicht()));
					row.add(""+dpg.korrektesMatching());
					rows.add(row);
				}
				final String[] dataType = { "NUMERIC", "NUMERIC", "STRING"};
				DefaultTableModel model = new DefaultTableModel(rows, colNames){
					@Override
					public boolean isCellEditable(int row, int column) {
						//all cells false
						return false;
					}
					@Override
					public Class<?> getColumnClass(int columnIndex) {
						if (dataType[columnIndex].equalsIgnoreCase("NUMERIC")) {
							return Integer.class;
						}
						else if (dataType[columnIndex].equalsIgnoreCase("DOUBLE")) {
							return Double.class;
						}
						return super.getColumnClass(columnIndex);
					}
				};
				ausgabeTabelle.setModel(model);
				ausgabeTabelle.setAutoscrolls(true);
			}
		}
		
	}
	private double differenz (double a, double b) {
		double rueckgabe;
		if(a > b)
			rueckgabe = a-b;
		else if (a == b)
			rueckgabe = 0;
		else
			rueckgabe = b-a;
		return rueckgabe;
	}
	private Vector getBewertung(double[][] bewertung, int i) {
		double wert1, wert2;
		Vector row = new Vector();
		// Startknoten
		row.add(i+1);
		// Summe Kanten / Anzahl
		row.add(Double.parseDouble(""+bewertung[i][1]/bewertung[i][0]));
		// 1. - 2.
		if(bewertung[i][0]-1 != 0)
			row.add(Double.parseDouble(""+(bewertung[i][2] - bewertung[i][3])));
		else
			row.add(Double.parseDouble("0.0"));
		// 1. - kleinste Kante
		if(bewertung[i][0]-1 != 0)
			row.add(Double.parseDouble(""+(bewertung[i][2] - bewertung[i][5])));
		else
			row.add(Double.parseDouble("0.0"));
		// Differenz 1.-2. & 1. - kleinste Kante
		if(bewertung[i][0]-1 != 0)
			wert1 = (bewertung[i][2] - bewertung[i][5]);
		else
			wert1 = 0;
		if(bewertung[i][0]-1 != 0)
			wert2 = bewertung[i][2] - bewertung[i][3];
		else
			wert2 = 0;
		row.add(Double.parseDouble(""+(differenz(wert1, wert2))));
		// 1. - (2.+3./2)
		if(bewertung[i][0]-2 > 0)
			row.add(Double.parseDouble(""+(bewertung[i][2] - ((bewertung[i][3]+bewertung[i][4])/2))));
		else if(bewertung[i][0]-1 > 0)
			row.add(Double.parseDouble(""+(bewertung[i][2] - ((bewertung[i][3])))));
		else
			row.add(Double.parseDouble("0.0"));
		// Differenz der linken beiden
		if(bewertung[i][0]-1 != 0)
			wert1 = bewertung[i][2] - bewertung[i][3];
		else
			wert1 = 0;
		if(bewertung[i][0]-2 > 0)
			wert2 = (bewertung[i][2] - (bewertung[i][3]+bewertung[i][4])/2);
		else if(bewertung[i][0]-1 > 0)
			wert2 = (bewertung[i][2] - (bewertung[i][3]));
		else
			wert2 = 0;
		row.add(Double.parseDouble(""+(differenz(wert1, wert2))));
		// Differenz 1.-2. und 1. - restl. Kanten
		if(bewertung[i][0]-1 != 0)
			wert1 = bewertung[i][2] - bewertung[i][3];
		else
			wert1 = 0;
		if(bewertung[i][0]-1 > 0)
			wert2 = ((bewertung[i][2] - ((bewertung[i][1]-bewertung[i][2])/(bewertung[i][0]-1))));
		else
			wert2 = 0;
		row.add(Double.parseDouble(""+(differenz(wert1, wert2))));
		// Differenz 1.-2. und 1.-3.
		if(bewertung[i][0]-1 != 0)
			wert1 = bewertung[i][2] - bewertung[i][3];
		else
			wert1 = 0;
		if(bewertung[i][0]-2 > 0)
			wert2 = bewertung[i][3] - bewertung[i][4];
		else
			wert2 = 0;
		row.add(Double.parseDouble(""+(differenz(wert1, wert2))));
		// 1.-2. + 1.-3. / 2
		if(bewertung[i][0]-1 != 0)
			wert1 = bewertung[i][2] - bewertung[i][3];
		else
			wert1 = 0;
		if(bewertung[i][0]-2 > 0)
			wert2 = bewertung[i][3] - bewertung[i][4];
		else
			wert2 = 0;
		row.add(Double.parseDouble(""+((wert1 + wert2)/2)));
		// 1. - (Summe r. Kanten / Anz. r. Kanten)
		if(bewertung[i][0]-1 > 0)
			row.add(Double.parseDouble(""+(bewertung[i][2] - ((bewertung[i][1]-bewertung[i][2])/(bewertung[i][0]-1)))));
		else
			row.add(Double.parseDouble("0.0"));
		// 1. - Summe Kanten / Anzahl
		if(bewertung[i][0]-1 > 0)
			row.add(Double.parseDouble(""+(bewertung[i][2] - (bewertung[i][1]/(bewertung[i][0])))));
		else
			row.add(Double.parseDouble("0.0"));
		return row;
	}
}














