package Beispiele;

import GraphKlassen.Graph;

public class Beispiele {
	public Graph getGraphbsp1car() {
		Graph g;
		g = new Graph(4);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		return g;
	}
	public Graph getGraphbsp2car() {
		Graph g;
		g = new Graph(10);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(1,4,1);
		g.insertKante(1,5,1);
		g.insertKante(1,6,1);
		g.insertKante(1,7,1);
		g.insertKante(1,8,1);
		g.insertKante(1,9,1);
		g.insertKante(1,10,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(6,7,1);
		g.insertKante(7,8,1);
		g.insertKante(8,9,1);
		g.insertKante(9,10,1);
		g.insertKante(10,2,1);
		return g;
	}
	public Graph getGraphbsp3car() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(2,4,1);
		g.insertKante(3,5,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		return g;
	}
	public Graph getGraphbsp4car() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(2,4,1);
		g.insertKante(3,4,1);
		g.insertKante(3,5,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		return g;
	}
	public Graph getGraphbsp5car() {
		Graph g;
		g = new Graph(16);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(1,4,1);
		g.insertKante(2,3,1);
		g.insertKante(2,4,1);
		g.insertKante(3,4,1);
		g.insertKante(3,5,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(7,8,1);
		g.insertKante(7,9,1);
		g.insertKante(7,10,1);
		g.insertKante(8,9,1);
		g.insertKante(8,10,1);
		g.insertKante(9,10,1);
		g.insertKante(9,11,1);
		g.insertKante(10,11,1);
		g.insertKante(11,6,1);
		g.insertKante(12,13,1);
		g.insertKante(12,14,1);
		g.insertKante(12,15,1);
		g.insertKante(13,14,1);
		g.insertKante(13,15,1);
		g.insertKante(14,15,1);
		g.insertKante(14,16,1);
		g.insertKante(15,16,1);
		g.insertKante(16,6,1);
		return g;
	}
	public Graph getGraphbsp6car() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(3,5,1);
		g.insertKante(3,6,1);
		return g;
	}
	public Graph getGraphbsp7car() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(2,3,1);
		g.insertKante(2,4,1);
		g.insertKante(3,5,1);
		g.insertKante(4,5,1);
		g.insertKante(4,6,1);
		return g;
	}
	public Graph getGraphbsp8car() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(1,4,1);
		g.insertKante(2,3,1);
		g.insertKante(2,4,1);
		g.insertKante(3,5,1);
		g.insertKante(4,5,1);
		return g;
	}
	public Graph getGraphbsp9car() {
		Graph g;
		g = new Graph(16);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(1,4,1);
		g.insertKante(2,3,1);
		g.insertKante(2,5,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(7,8,1);
		g.insertKante(7,9,1);
		g.insertKante(7,10,1);
		g.insertKante(8,9,1);
		g.insertKante(8,11,1);
		g.insertKante(9,10,1);
		g.insertKante(10,11,1);
		g.insertKante(6,11,1);
		g.insertKante(12,13,1);
		g.insertKante(12,14,1);
		g.insertKante(12,15,1);
		g.insertKante(13,14,1);
		g.insertKante(13,16,1);
		g.insertKante(14,15,1);
		g.insertKante(15,16,1);
		g.insertKante(6,16,1);
		return g;
	}	
	public Graph getGraphbsp10car() {
		Graph g;
		g = new Graph(10);
		g.insertKante(1,2,1);
		g.insertKante(1,10,1);
		g.insertKante(1,5,1);
		g.insertKante(2,3,1);
		g.insertKante(2,6,1);
		g.insertKante(3,7,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(4,8,1);
		g.insertKante(5,9,1);
		g.insertKante(6,8,1);
		g.insertKante(6,9,1);
		g.insertKante(7,9,1);
		g.insertKante(7,10,1);
		g.insertKante(8,10,1);
		return g;
	}
	public Graph getGraphbsp11car() {
		Graph g;
		g = new Graph(12);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(1,4,1);
		g.insertKante(1,5,1);
		g.insertKante(2,3,1);
		g.insertKante(2,4,1);
		g.insertKante(2,5,1);
		g.insertKante(2,6,1);
		g.insertKante(3,4,1);
		g.insertKante(3,5,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(6,7,1);
		g.insertKante(7,8,1);
		g.insertKante(7,12,1);
		g.insertKante(8,9,1);
		g.insertKante(8,10,1);
		g.insertKante(8,11,1);
		g.insertKante(8,12,1);
		g.insertKante(9,10,1);
		g.insertKante(9,11,1);
		g.insertKante(9,12,1);
		g.insertKante(10,11,1);
		g.insertKante(10,12,1);
		g.insertKante(11,12,1);
		return g;
	}
	public Graph getGraphbsp12car() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(4,6,1);
		g.insertKante(5,6,1);
		return g;
	}
	public Graph getGraphbsp1wei() {
		Graph g;
		g = new Graph(4);
		g.insertKante(1,2,5);
		g.insertKante(2,3,4);
		g.insertKante(3,4,2);
		g.insertKante(4,1,10);
		return g;
	}
	public Graph getGraphbsp2wei() {
		Graph g;
		g = new Graph(6);
		g.insertKante(1,2,2);
		g.insertKante(1,5,3);
		g.insertKante(2,3,7);
		g.insertKante(2,6,1);
		g.insertKante(3,6,4);
		g.insertKante(3,4,9);
		g.insertKante(4,5,8);
		g.insertKante(4,6,4);
		return g;
	}
	public Graph getGraphbsp3wei() {
		Graph g;
		int anzahl = 100;
		g = new Graph(anzahl);
		for(int i = 1; i < anzahl+1; i++) {
			for(int j = 1; j < anzahl+1; j++) {
				if(i!=j && i < j) {
					int gewicht = (int) (Math.random() * 10);
					if(gewicht > 0)
						g.insertKante(i, j, gewicht);
				}
			}
		}
		return g;
	}
	public Graph getGraphbsp4wei() {
		Graph g;
		int anzahl = 3;
		g = new Graph(anzahl*anzahl);
		for(int i = 1; i < anzahl*anzahl+1; i++) {
			if(i+anzahl < anzahl*anzahl+1) {
				int gewicht = (int) (Math.random() * 10);
				if(gewicht > 0)
					g.insertKante(i, i+anzahl, gewicht);
				else
					g.insertKante(i, i+anzahl, 2);
			}
			if(i%anzahl != 0) {
				int gewicht = (int) (Math.random() * 10);
				if(gewicht > 0)
					g.insertKante(i, i+1, gewicht);
				else
					g.insertKante(i, i+1, 2);
			}
		}
		return g;
	}
	public Graph getGraphbsp5wei() {
		Graph g;
		g = new Graph(48);
		g.insertKante(1,2,3879);
		g.insertKante(1,3,6269);
		g.insertKante(1,4,1980);
		g.insertKante(1,16,6488);
		g.insertKante(1,22,6534);
		g.insertKante(1,26,2450);
		g.insertKante(1,29,4742);
		g.insertKante(1,34,5724);
		g.insertKante(1,41,5811);
		g.insertKante(3,4,1848);
		g.insertKante(3,26,2272);
		g.insertKante(3,29,4318);
		g.insertKante(3,34,5214);
		g.insertKante(3,41,5269);
		g.insertKante(5,26,1932);
		g.insertKante(5,42,2912);
		g.insertKante(6,7,8612);
		g.insertKante(6,8,7805);
		g.insertKante(6,9,7802);
		g.insertKante(6,10,4194);
		g.insertKante(6,11,7295);
		g.insertKante(6,12,7640);
		g.insertKante(6,13,6908);
		g.insertKante(6,14,6645);
		g.insertKante(6,15,7762);
		g.insertKante(6,16,7031);
		g.insertKante(6,18,8530);
		g.insertKante(6,20,7795);
		g.insertKante(6,21,7008);
		g.insertKante(6,22,7166);
		g.insertKante(6,23,7020);
		g.insertKante(6,24,4994);
		g.insertKante(6,25,6566);
		g.insertKante(6,26,3101);
		g.insertKante(6,28,8667);
		g.insertKante(6,29,5247);
		g.insertKante(6,31,8359);
		g.insertKante(6,32,6278);
		g.insertKante(6,33,7939);
		g.insertKante(6,34,6355);
		g.insertKante(6,35,3170);
		g.insertKante(6,36,8484);
		g.insertKante(6,38,8138);
		g.insertKante(6,39,6254);
		g.insertKante(6,40,7564);
		g.insertKante(6,41,6208);
		g.insertKante(6,42,4644);
		g.insertKante(6,44,8459);
		g.insertKante(6,45,3464);
		g.insertKante(6,46,8070);
		g.insertKante(6,47,7346);
		g.insertKante(6,48,5626);
		g.insertKante(7,8,7728);
		g.insertKante(7,9,7696);
		g.insertKante(7,10,4004);
		g.insertKante(7,11,7147);
		g.insertKante(7,12,7485);
		g.insertKante(7,13,6750);
		g.insertKante(7,14,6518);
		g.insertKante(7,15,7618);
		g.insertKante(7,16,6980);
		g.insertKante(7,18,8357);
		g.insertKante(7,20,7610);
		g.insertKante(7,21,6816);
		g.insertKante(7,22,7094);
		g.insertKante(7,23,6890);
		g.insertKante(7,24,4775);
		g.insertKante(7,25,6422);
		g.insertKante(7,26,2974);
		g.insertKante(7,29,5183);
		g.insertKante(7,31,8218);
		g.insertKante(7,32,6070);
		g.insertKante(7,33,7775);
		g.insertKante(7,34,6272);
		g.insertKante(7,35,2899);
		g.insertKante(7,38,8019);
		g.insertKante(7,39,6074);
		g.insertKante(7,40,7444);
		g.insertKante(7,41,6186);
		g.insertKante(7,42,4510);
		g.insertKante(7,44,8300);
		g.insertKante(7,45,3165);
		g.insertKante(7,46,7916);
		g.insertKante(7,47,7167);
		g.insertKante(7,48,5487);
		g.insertKante(8,16,6724);
		g.insertKante(8,22,6762);
		g.insertKante(8,26,2485);
		g.insertKante(8,29,4903);
		g.insertKante(8,34,5915);
		g.insertKante(8,41,6032);
		g.insertKante(9,16,6586);
		g.insertKante(9,22,6646);
		g.insertKante(9,26,2559);
		g.insertKante(9,29,4831);
		g.insertKante(9,34,5834);
		g.insertKante(9,41,5886);
		g.insertKante(9,42,4021);
		g.insertKante(10,26,1676);
		g.insertKante(11,14,5524);
		g.insertKante(11,23,5840);
		g.insertKante(11,25,5440);
		g.insertKante(11,26,2512);
		g.insertKante(11,29,4397);
		g.insertKante(11,34,5321);
		g.insertKante(11,35,2429);
		g.insertKante(11,41,5255);
		g.insertKante(11,42,3816);
		g.insertKante(11,48,4646);
		g.insertKante(12,13,5988);
		g.insertKante(12,14,5785);
		g.insertKante(12,23,6116);
		g.insertKante(12,24,4224);
		g.insertKante(12,25,5697);
		g.insertKante(12,26,2633);
		g.insertKante(12,29,4604);
		g.insertKante(12,34,5571);
		g.insertKante(12,35,2550);
		g.insertKante(12,39,5384);
		g.insertKante(12,41,5500);
		g.insertKante(12,42,3997);
		g.insertKante(12,45,2783);
		g.insertKante(12,48,4866);
		g.insertKante(13,14,5210);
		g.insertKante(13,25,5145);
		g.insertKante(13,26,2422);
		g.insertKante(13,29,4119);
		g.insertKante(13,34,4988);
		g.insertKante(13,35,2456);
		g.insertKante(13,42,3634);
		g.insertKante(13,48,4406);
		g.insertKante(14,26,2271);
		g.insertKante(14,29,4023);
		g.insertKante(14,34,4867);
		g.insertKante(14,42,3468);
		g.insertKante(14,48,4230);
		g.insertKante(15,16,6368);
		g.insertKante(15,22,6457);
		g.insertKante(15,23,6233);
		g.insertKante(15,25,5793);
		g.insertKante(15,26,2642);
		g.insertKante(15,29,4710);
		g.insertKante(15,34,5697);
		g.insertKante(15,35,2468);
		g.insertKante(15,40,6747);
		g.insertKante(15,41,5658);
		g.insertKante(15,42,4046);
		g.insertKante(15,45,2691);
		g.insertKante(15,48,4939);
		g.insertKante(16,41,5526);
		g.insertKante(17,18,8683);
		g.insertKante(17,20,7960);
		g.insertKante(17,21,7182);
		g.insertKante(17,22,7224);
		g.insertKante(17,23,7133);
		g.insertKante(17,24,5195);
		g.insertKante(17,25,6694);
		g.insertKante(17,26,3217);
		g.insertKante(17,27,9082);
		g.insertKante(17,28,8833);
		g.insertKante(17,29,5300);
		g.insertKante(17,30,8905);
		g.insertKante(17,31,8482);
		g.insertKante(17,32,6467);
		g.insertKante(17,33,8084);
		g.insertKante(17,34,6425);
		g.insertKante(17,35,3415);
		g.insertKante(17,36,8646);
		g.insertKante(17,38,8239);
		g.insertKante(17,39,6417);
		g.insertKante(17,40,7667);
		g.insertKante(17,41,6218);
		g.insertKante(17,42,4766);
		g.insertKante(17,43,8989);
		g.insertKante(17,44,8598);
		g.insertKante(17,45,3734);
		g.insertKante(17,46,8205);
		g.insertKante(17,47,7506);
		g.insertKante(17,48,5751);
		g.insertKante(18,20,7537);
		g.insertKante(18,21,6748);
		g.insertKante(18,22,7036);
		g.insertKante(18,23,6828);
		g.insertKante(18,24,4719);
		g.insertKante(18,25,6361);
		g.insertKante(18,26,2940);
		g.insertKante(18,29,5140);
		g.insertKante(18,32,6006);
		g.insertKante(18,33,7703);
		g.insertKante(18,34,6219);
		g.insertKante(18,35,2850);
		g.insertKante(18,38,7950);
		g.insertKante(18,39,6012);
		g.insertKante(18,40,7379);
		g.insertKante(18,41,6140);
		g.insertKante(18,42,4464);
		g.insertKante(18,45,3111);
		g.insertKante(18,46,7844);
		g.insertKante(18,47,7098);
		g.insertKante(18,48,5434);
		g.insertKante(19,20,7901);
		g.insertKante(19,21,7111);
		g.insertKante(19,22,7239);
		g.insertKante(19,23,7106);
		g.insertKante(19,24,5088);
		g.insertKante(19,25,6653);
		g.insertKante(19,26,3157);
		g.insertKante(19,28,8781);
		g.insertKante(19,29,5304);
		g.insertKante(19,30,8838);
		g.insertKante(19,31,8459);
		g.insertKante(19,32,6379);
		g.insertKante(19,33,8041);
		g.insertKante(19,34,6425);
		g.insertKante(19,35,3262);
		g.insertKante(19,36,8595);
		g.insertKante(19,38,8231);
		g.insertKante(19,39,6348);
		g.insertKante(19,40,7653);
		g.insertKante(19,41,6261);
		g.insertKante(19,42,4714);
		g.insertKante(19,44,8564);
		g.insertKante(19,45,3565);
		g.insertKante(19,46,8171);
		g.insertKante(19,47,7448);
		g.insertKante(19,48,5704);
		g.insertKante(20,21,6200);
		g.insertKante(20,23,6200);
		g.insertKante(20,24,4431);
		g.insertKante(20,25,5803);
		g.insertKante(20,26,2750);
		g.insertKante(20,29,4629);
		g.insertKante(20,32,5560);
		g.insertKante(20,34,5608);
		g.insertKante(20,35,2833);
		g.insertKante(20,39,5534);
		g.insertKante(20,41,5469);
		g.insertKante(20,42,4109);
		g.insertKante(20,45,3096);
		g.insertKante(20,47,6495);
		g.insertKante(20,48,4975);
		g.insertKante(21,24,4096);
		g.insertKante(21,25,5208);
		g.insertKante(21,26,2532);
		g.insertKante(21,29,4097);
		g.insertKante(21,32,5067);
		g.insertKante(21,35,2751);
		g.insertKante(21,39,5014);
		g.insertKante(21,42,3724);
		g.insertKante(21,45,3010);
		g.insertKante(21,48,4482);
		g.insertKante(22,26,2287);
		g.insertKante(22,29,4496);
		g.insertKante(22,41,5528);
		g.insertKante(23,26,2388);
		g.insertKante(23,29,4262);
		g.insertKante(23,34,5154);
		g.insertKante(23,41,5120);
		g.insertKante(23,42,3658);
		g.insertKante(23,48,4466);
		g.insertKante(24,26,1981);
		g.insertKante(24,35,2502);
		g.insertKante(24,45,2744);
		g.insertKante(25,26,2289);
		g.insertKante(25,29,3929);
		g.insertKante(25,35,2290);
		g.insertKante(25,42,3448);
		g.insertKante(25,48,4186);
		g.insertKante(27,28,8726);
		g.insertKante(27,29,5257);
		g.insertKante(27,30,8789);
		g.insertKante(27,31,8396);
		g.insertKante(27,32,6359);
		g.insertKante(27,33,7989);
		g.insertKante(27,34,6370);
		g.insertKante(27,35,3294);
		g.insertKante(27,36,8542);
		g.insertKante(27,38,8164);
		g.insertKante(27,39,6320);
		g.insertKante(27,40,7593);
		g.insertKante(27,41,6191);
		g.insertKante(27,42,4694);
		g.insertKante(27,44,8505);
		g.insertKante(27,45,3601);
		g.insertKante(27,46,8114);
		g.insertKante(27,47,7407);
		g.insertKante(27,48,5674);
		g.insertKante(28,29,5193);
		g.insertKante(28,32,6134);
		g.insertKante(28,33,7816);
		g.insertKante(28,34,6286);
		g.insertKante(28,35,2999);
		g.insertKante(28,36,8348);
		g.insertKante(28,38,8042);
		g.insertKante(28,39,6126);
		g.insertKante(28,40,7469);
		g.insertKante(28,41,6176);
		g.insertKante(28,42,4549);
		g.insertKante(28,44,8337);
		g.insertKante(28,45,3275);
		g.insertKante(28,46,7952);
		g.insertKante(28,47,7215);
		g.insertKante(28,48,5525);
		g.insertKante(30,32,6224);
		g.insertKante(30,33,7844);
		g.insertKante(30,34,6267);
		g.insertKante(30,35,3186);
		g.insertKante(30,36,8385);
		g.insertKante(30,39,6193);
		g.insertKante(30,40,7464);
		g.insertKante(30,41,6105);
		g.insertKante(30,42,4599);
		g.insertKante(30,45,3483);
		g.insertKante(30,46,7970);
		g.insertKante(30,47,7266);
		g.insertKante(30,48,5565);
		g.insertKante(31,34,6174);
		g.insertKante(31,35,2525);
		g.insertKante(31,38,7877);
		g.insertKante(31,40,7300);
		g.insertKante(31,41,6164);
		g.insertKante(31,42,4341);
		g.insertKante(31,45,2751);
		g.insertKante(31,48,5314);
		g.insertKante(32,35,2720);
		g.insertKante(32,42,3376);
		g.insertKante(32,45,2979);
		g.insertKante(32,48,4028);
		g.insertKante(33,34,5780);
		g.insertKante(33,35,2679);
		g.insertKante(33,39,5600);
		g.insertKante(33,40,6860);
		g.insertKante(33,41,5698);
		g.insertKante(33,42,4158);
		g.insertKante(33,45,2924);
		g.insertKante(33,48,5058);
		g.insertKante(36,39,5995);
		g.insertKante(36,40,7315);
		g.insertKante(36,41,6054);
		g.insertKante(36,42,4451);
		g.insertKante(36,45,3191);
		g.insertKante(36,46,7787);
		g.insertKante(36,47,7063);
		g.insertKante(36,48,5408);
		g.insertKante(37,38,8227);
		g.insertKante(37,39,6327);
		g.insertKante(37,40,7647);
		g.insertKante(37,41,6271);
		g.insertKante(37,42,4699);
		g.insertKante(37,44,8553);
		g.insertKante(37,45,3517);
		g.insertKante(37,46,8160);
		g.insertKante(37,47,7430);
		g.insertKante(37,48,5691);
		g.insertKante(38,40,7151);
		g.insertKante(38,41,6096);
		g.insertKante(38,42,4205);
		g.insertKante(38,48,5167);
		g.insertKante(39,42,3333);
		g.insertKante(39,45,2757);
		g.insertKante(39,48,4003);
		g.insertKante(40,41,5617);
		g.insertKante(40,42,3919);
		g.insertKante(40,48,4806);
		g.insertKante(43,45,3630);
		g.insertKante(43,46,8013);
		g.insertKante(43,47,7328);
		g.insertKante(43,48,5614);
		g.insertKante(44,45,2956);
		g.insertKante(44,46,7797);
		g.insertKante(44,48,5384);
		g.insertKante(46,48,5137);
		g.insertKante(47,48,4690);

		return g;
	}
	public Graph getGraphbsp1edm() {
		Graph g;
		g = new Graph(8);
		g.insertKante(1,2,1);
		g.insertKante(1,3,1);
		g.insertKante(1,4,1);
		g.insertKante(2,5,1);
		g.insertKante(3,6,1);
		g.insertKante(4,7,1);
		g.insertKante(5,8,1);
		g.insertKante(6,8,1);
		g.insertKante(7,8,1);
		return g;
	}
	public Graph getMatchingsp1edm() {
		Graph g;
		g = new Graph(8);
		g.insertKante(2,5,1);
		g.insertKante(3,6,1);
		g.insertKante(4,7,1);
		return g;
	}
	public Graph getGraphbsp2edm() {
		Graph g;
		g = new Graph(8);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(6,7,1);
		g.insertKante(7,8,1);
		g.insertKante(1,8,1);
		g.insertKante(1,5,1);
		g.insertKante(3,7,1);
		return g;
	}
	public Graph getMatchingsp2edm() {
		Graph g;
		g = new Graph(8);
		g.insertKante(1,5,1);
		g.insertKante(3,7,1);
		return g;
	}
	public Graph getGraphbsp3edm() {
		Graph g;
		g = new Graph(7);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(6,7,1);
		g.insertKante(7,1,1);
		return g;
	}
	public Graph getMatchingsp3edm() {
		Graph g;
		g = new Graph(7);
		g.insertKante(2,3,1);
		g.insertKante(4,5,1);
		g.insertKante(6,7,1);
		return g;
	}
	public Graph getGraphbsp4edm() {
		Graph g;
		g = new Graph(8);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(6,7,1);
		g.insertKante(3,7,1);
		g.insertKante(7,8,1);
		return g;
	}
	public Graph getMatchingsp4edm() {
		Graph g;
		g = new Graph(8);
		g.insertKante(1,2,1);
		g.insertKante(4,5,1);
		g.insertKante(7,8,1);
		return g;
	}
	public Graph getGraphbsp5edm() {
		Graph g;
		g = new Graph(10);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		g.insertKante(3,4,1);
		g.insertKante(4,5,1);
		g.insertKante(5,6,1);
		g.insertKante(6,7,1);
		g.insertKante(6,8,1);
		g.insertKante(3,8,1);
		g.insertKante(8,9,1);
		g.insertKante(7,10,1);
		return g;
	}
	public Graph getMatchingsp5edm() {
		Graph g;
		g = new Graph(10);
		g.insertKante(1,2,1);
		g.insertKante(4,5,1);
		g.insertKante(6,7,1);
		g.insertKante(8,9,1);
		return g;
	}
	public Graph getGraphbsp6edm() {
		Graph g;
		g = new Graph(4);
		g.insertKante(1,2,1);
		g.insertKante(2,3,1);
		//g.insertKante(2,4,1);
		g.insertKante(3,4,1);
		return g;
	}
	public Graph getMatchingsp6edm() {
		Graph g;
		g = new Graph(4);
		g.insertKante(2,3,1);
		return g;
	}
}
