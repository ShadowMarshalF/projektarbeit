package GraphKlassen;

public class Kante {
	private int knoten1;
	private int knoten2;
	private int gewicht;
	public Kante(int knoten1, int knoten2, int gewicht) {
		this.knoten1 = knoten1;
		this.knoten2 = knoten2;
		this.gewicht = gewicht;
	}
	public int getKnoten1() {
		return knoten1;
	}
	public int getKnoten2() {
		return knoten2;
	}
	public int getGewicht() {
		return gewicht;
	}
	public void setGewicht(int gewicht) {
		this.gewicht = gewicht;
	}
	public String toString() {
		return "<td>" + knoten1 + " </td><td> " + knoten2 + "</td><td> " + gewicht + "</td>";
	}
	public String toString2() {
		return "<td>" + knoten1 + " </td><td> " + knoten2 + "</td> ";
	}
}
