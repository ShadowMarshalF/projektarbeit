package GraphKlassen;
import java.util.ArrayList;

import ApproxAlgorithmen.MaxHeap;

public class Graph {
	private int anzahl_knoten;
	private int graph[][];
	private ArrayList<Kante> kantenliste = new ArrayList<Kante>();
	public Graph(int anzahl_knoten) {
		this.anzahl_knoten = anzahl_knoten;
		graph = new int[anzahl_knoten][anzahl_knoten];
	}
	public Graph(int[][] graph, int anzahl_knoten) {
		this.anzahl_knoten = anzahl_knoten;
		this.graph = new int[anzahl_knoten][anzahl_knoten];
		for(int i = 0; i < this.anzahl_knoten; i++) {
			for(int j = 0; j < this.anzahl_knoten; j++) {
				if(graph[i][j] > 0 && i > j) {
					insertKante((i+1), (j+1), graph[i][j]);
				}
			}
		}
	}
	public void insertKante(int knoten1, int knoten2, int gewicht) {
		kantenliste.add(new Kante(knoten1, knoten2, gewicht));
		graph[knoten1-1][knoten2-1] = gewicht;
		graph[knoten2-1][knoten1-1] = gewicht;
	}
	public void updateKante(int knoten1, int knoten2, int gewicht) {
		
		graph[knoten1-1][knoten2-1] = gewicht;
		graph[knoten2-1][knoten1-1] = gewicht;
	}
	public String writeTable() {
		String ausgabe="";
		for(int i = 0; i<anzahl_knoten; i++) {
			for(int j = 0; j<anzahl_knoten; j++) {
				ausgabe += graph[i][j]+" ";
			}
			ausgabe+="\n";
		}
		return ausgabe;
	}
	public int getAnzahlKnoten() {
		return anzahl_knoten;
	}
	public int[][] getGraph(){
		return graph;
	}
	public String writeTableHTML() {
		String ausgabe="<table border=\"1\" align=\"center\"><tr><td></td>";
		for(int i = 0; i<anzahl_knoten; i++) 
			ausgabe+= "<td>" + (i+1) + "</td>";
		ausgabe+="</tr>";
		for(int i = 0; i<anzahl_knoten; i++) {
			ausgabe+= "<tr><td>" + (i+1) + "</td>";
			for(int j = 0; j<anzahl_knoten; j++) {
				ausgabe += "<td>" + graph[i][j]+"</td>";
			}
			ausgabe+= "</tr>";
		}
		ausgabe+="</table>";
		return ausgabe;
	}
	public ArrayList<Kante> getKantenliste() {
		return kantenliste;
	}
	// Wert 0: Anzahl der Kanten
	// Wert 1: Summe aller Kanten
	// Wert 2: 1. groesste Kante
	// Wert 3: 2. groesste Kante
	// Wert 4: 3. groesste Kante
	// Wert 5: kleinste Kante
	public double[][] getKnotenDurchschnitte() {
		double[][] rueckgabe = new double[this.anzahl_knoten][6];
		for(int i = 0; i < this.anzahl_knoten; i++) {
			int klKante = Integer.MAX_VALUE;
			rueckgabe[i][1] = 0;
			int anzahl = 0;
			MaxHeap Kanten;
			Kanten = new MaxHeap(this.anzahl_knoten*this.anzahl_knoten);
			for(int j = 0; j < this.anzahl_knoten; j++) {
				int wert = this.graph[i][j];
				if(wert > 0) {
					anzahl += 1;
					rueckgabe[i][1] += this.graph[i][j];
					if(wert < klKante)
						klKante = wert;
					Kanten.insert(new Kante(i,j,wert));
				}
				
			}
			rueckgabe[i][0] = anzahl;
			Kante wert = Kanten.extractMax();
			if(wert != null)
				rueckgabe[i][2] = wert.getGewicht();
			else
				rueckgabe[i][2] = 0;
			wert = Kanten.extractMax();
			if(wert != null)
				rueckgabe[i][3] = wert.getGewicht();
			else
				rueckgabe[i][3] = 0;
			wert = Kanten.extractMax();
			if(wert != null)
				rueckgabe[i][4] = wert.getGewicht();
			else
				rueckgabe[i][4] = 0;
			rueckgabe[i][5] = klKante;
		}
		return rueckgabe;
	}
}





