package GraphKlassen;
import java.io.*;
public class GraphLaden{
	FileReader fr;
	Graph g;
	int anzahlKnoten;
	public GraphLaden(File file) throws IOException{
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			anzahlKnoten = Integer.parseInt(br.readLine());
			g = new Graph(anzahlKnoten);
			String zeile;
			while((zeile = br.readLine()) != null){
				String[] split = zeile.split(",");
				int knoten1 = Integer.parseInt(split[0]);
				int knoten2 = Integer.parseInt(split[1]);
				int gewicht = Integer.parseInt(split[2]);
				g.insertKante(knoten1, knoten2, gewicht);
			}
			br.close();
		}catch(IOException e) {
			System.out.println("Ein Fehler beim Laden der Datei ist aufgetreten");
		}
	}
	public Graph getGraph() {
		return g;
	}
}
